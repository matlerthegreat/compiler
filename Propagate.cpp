#include "Command.h"

std::optional<std::array<StrongReference, 2>> Command::calc_nul()
{
	return {{m_dest, m_propagated[0]}};
}

std::optional<std::array<StrongReference, 2>> Command::calc_add()
{
	if (!is_num(m_propagated[0]) || !is_num(m_propagated[1]))
		return std::nullopt;

	auto a = std::stoull(m_propagated[0].uuid);
	auto b = std::stoull(m_propagated[1].uuid);

	auto c = a + b;

	return {{m_dest, Reference{std::to_string(c)}}};
}

std::optional<std::array<StrongReference, 2>> Command::calc_sub()
{
	if (!is_num(m_propagated[0]) || !is_num(m_propagated[1]))
		return std::nullopt;

	auto a = std::stoull(m_propagated[0].uuid);
	auto b = std::stoull(m_propagated[1].uuid);

	auto c = a - b;
	if (b > a)
		c = 0;

	return {{m_dest, Reference{std::to_string(c)}}};
}

std::optional<std::array<StrongReference, 2>> Command::calc_mul()
{
	if (!is_num(m_propagated[0]) || !is_num(m_propagated[1]))
		return std::nullopt;

	auto a = std::stoull(m_propagated[0].uuid);
	auto b = std::stoull(m_propagated[1].uuid);

	auto c = a * b;

	return {{m_dest, Reference{std::to_string(c)}}};
}

std::optional<std::array<StrongReference, 2>> Command::calc_div()
{
	if (!is_num(m_propagated[0]) || !is_num(m_propagated[1]))
		return std::nullopt;

	auto a = std::stoull(m_propagated[0].uuid);
	auto b = std::stoull(m_propagated[1].uuid);

	auto c = a / b;
	if (b == 0)
		c = 0;

	return {{m_dest, Reference{std::to_string(c)}}};
}

std::optional<std::array<StrongReference, 2>> Command::calc_mod()
{
	if (!is_num(m_propagated[0]) || !is_num(m_propagated[1]))
		return std::nullopt;

	auto a = std::stoull(m_propagated[0].uuid);
	auto b = std::stoull(m_propagated[1].uuid);

	auto c = a % b;
	if (b == 0)
		c = 0;

	m_src.operands[1] = Reference{""};
	m_src.operands[0] = Reference{std::to_string(c)};
	m_src.op = Operator::Nul;
	m_propagated = m_src.operands;

	return {{m_dest, m_propagated[0]}};
}

std::optional<std::array<StrongReference, 2>> Command::get_replacement()
{
	switch (m_src.op) {
		case Operator::Nul:
			return calc_nul();
		case Operator::Add:
			return calc_add();
		case Operator::Sub:
			return calc_sub();
		case Operator::Mul:
			return calc_mul();
		case Operator::Div:
			return calc_div();
		case Operator::Mod:
			return calc_mod();
		default:
			return std::nullopt;
	}
}

void Command::replace(std::array<StrongReference, 2> replacement)
{
	if (is_same(m_propagated[0], replacement[0]))
		m_propagated[0] = replacement[1];
	else if (is_same(m_propagated[1], replacement[0]))
		m_propagated[1] = replacement[1];
	else
		return;

	switch (m_src.op) {
		case Operator::Wrt:
			m_src.operands[0] = m_propagated[0];
			break;

		case Operator::Add:
			if (is_num(m_propagated[0]) && std::stoull(m_propagated[0].uuid) <= 12)
				m_src.operands[0] = m_propagated[0];
			if (is_num(m_propagated[1]) && std::stoull(m_propagated[1].uuid) <= 12)
				m_src.operands[1] = m_propagated[1];

		case Operator::Sub:
			if (is_num(m_propagated[0]) && std::stoull(m_propagated[0].uuid) <= 12)
				m_src.operands[0] = m_propagated[0];
			if (is_num(m_propagated[1]) && std::stoull(m_propagated[1].uuid) <= 12)
				m_src.operands[1] = m_propagated[1];
		default:
			break;
	}
}
