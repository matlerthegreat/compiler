#include "Command.h"
#include "Block.h"
#include "Program.h"
#include <iostream>

std::vector<Command> generate_num(ULLONG num, Reference reg)
{
	std::vector<Command> result;
	if (num < 12) {
		result.push_back(Command({}, Expression{Operator::SUB, reg, reg}));
		for (auto i = 0; i < num; ++i)
			result.push_back(Command({}, Expression{Operator::INC, reg}));

		return result;
	}

	result = generate_num(num / 2, reg);
	result.push_back(Command({}, Expression{Operator::ADD, reg, reg}));

	if (num % 2)
		result.push_back(Command({}, Expression{Operator::INC, reg}));

	return result;
}

std::vector<Command> generate_index(Reference ref, std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map)
{
	size_t index = mem_map[ref.uuid][0];
	size_t begin = mem_map[ref.uuid][1];

	if (ref.offset) {
		std::vector<Command> commands;
		auto offset = *ref.offset;

		if (is_num(offset)) {
			commands = generate_num(index + std::stoull(offset.uuid) - begin);
		}
		else if (!reg_map[offset]) {
			auto num = generate_num(mem_map[offset.uuid][0]);
			commands.insert(commands.end(), num.begin(), num.end());
			offset = {"H"};
			commands.push_back(Command({}, Expression{Operator::LOAD, offset}));

			num = generate_num(index);
			commands.insert(commands.end(), num.begin(), num.end());
			commands.push_back(Command({}, Expression{Operator::ADD, Reference{"A"}, offset}));

			num = generate_num(begin, offset);
			commands.insert(commands.end(), num.begin(), num.end());
			commands.push_back(Command({}, Expression{Operator::SUB, Reference{"A"}, offset}));
		}
		else {
			auto num = generate_num(index);
			commands.insert(commands.end(), num.begin(), num.end());

			offset = {std::string() + reg_map[offset]};
			commands.push_back(Command({}, Expression{Operator::ADD, Reference{"A"}, offset}));

			num = generate_num(begin, {"H"});
			commands.insert(commands.end(), num.begin(), num.end());
			commands.push_back(Command({}, Expression{Operator::SUB, Reference{"A"}, {"H"}}));
		}

		return commands;
	}

	return generate_num(index);
}

void set_reg(std::unordered_map<StrongReference, char>& reg_map, Reference ref, char reg)
{
	for (auto& map: reg_map) {
		if (map.second == reg) {
			map.second = 0;
			break;
		}
	}

	if (ref.uuid.empty() || ref.offset)
		return;

	reg_map[ref] = reg;
}

Flow_graph Command::generate_nul(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const
{
	std::vector<Command> commands;
	Reference src = m_src.operands[0];
	if (is_num(src)) {
		set_reg(reg_map);
		commands = generate_num(std::stoull(src.uuid), {"B"});
		src = {"B"};
	}
	else if (!reg_map[src]) {
		commands = generate_index(src, reg_map, mem_map);
		set_reg(reg_map, src);
		src = {"B"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src}));
	}
	else
		src = {std::string() + reg_map[src]};

	Reference dest = m_dest;
	if (!reg_map[dest]) {
		auto index = generate_index(dest, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		commands.push_back(Command({}, Expression{Operator::STORE, src}));
	}
	else {
		dest = {std::string() + reg_map[dest]};
		commands.push_back(Command({}, Expression{Operator::COPY, dest, src}));
	}

	auto root = std::make_shared<Block>(commands);
	return {root, root};
}

Flow_graph Command::generate_add(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const
{
	auto dest = m_dest;
	if (!reg_map[dest])
		dest = {"B"};
	else
		dest = {std::string() + reg_map[m_dest]};

	auto src = m_src.operands;
	std::vector<Command> commands;
	if (is_num(src[0])) {
		auto num = generate_num(std::stoull(src[0].uuid));
		commands.insert(commands.end(), num.begin(), num.end());
		src[0] = dest;
		commands.push_back(Command({}, Expression{Operator::COPY, src[0], {"A"}}));
	}
	else if (!reg_map[src[0]]) {
		auto index = generate_index(src[0], reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		set_reg(reg_map, src[0]);
		src[0] = {"B"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else
		src[0] = {std::string() + reg_map[src[0]]};

	if (is_num(src[1])) {
		if (std::stoull(src[1].uuid) > 12) {
			auto num = generate_num(std::stoull(src[1].uuid));
			commands.insert(commands.end(), num.begin(), num.end());
			src[1] = {"A"};
		}
	}
	else if (!reg_map[src[1]]) {
		auto index = generate_index(src[1], reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());

		src[1] = {"A"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else
		src[1] = {std::string() + reg_map[src[1]]};

	if (dest == src[0]) {
		if (!is_num(src[1]) || std::stoull(src[1].uuid) > 12)
			commands.push_back(Command({}, Expression{Operator::ADD, dest, src[1]}));
		else {
			auto num = std::stoull(src[1].uuid);
			for (auto i = 0; i < num; ++i)
				commands.push_back(Command({}, Expression{Operator::INC, dest}));
		}
	}
	else if (dest == src[1]) {
		commands.push_back(Command({}, Expression{Operator::ADD, dest, src[0]}));
	}
	else {
		commands.push_back(Command({}, Expression{Operator::COPY, dest, src[0]}));
		if (!is_num(src[1]) || std::stoull(src[1].uuid) > 12)
			commands.push_back(Command({}, Expression{Operator::ADD, dest, src[1]}));
		else {
			auto num = std::stoull(src[1].uuid);
			for (auto i = 0; i < num; ++i)
				commands.push_back(Command({}, Expression{Operator::INC, dest}));
		}
	}

	if (dest.uuid == "B") {
		set_reg(reg_map, m_dest);
		auto index = generate_index(m_dest, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());

		commands.push_back(Command({}, Expression{Operator::STORE, dest}));
	}

	auto root = std::make_shared<Block>(commands);
	return {root, root};
}

Flow_graph Command::generate_sub(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const
{
	auto dest = m_dest;
	if (!reg_map[dest])
		dest = {"B"};
	else
		dest = {std::string() + reg_map[m_dest]};

	auto src = m_src.operands;
	std::vector<Command> commands;
	if (is_num(src[0])) {
		auto num = generate_num(std::stoull(src[0].uuid));
		commands.insert(commands.end(), num.begin(), num.end());
		src[0] = dest;
		commands.push_back(Command({}, Expression{Operator::COPY, src[0], {"A"}}));
	}
	else if (!reg_map[src[0]]) {
		auto index = generate_index(src[0], reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		set_reg(reg_map, src[0]);
		src[0] = {"B"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else
		src[0] = {std::string() + reg_map[src[0]]};

	if (is_num(src[1])) {
		if (std::stoull(src[1].uuid) > 12) {
			auto num = generate_num(std::stoull(src[1].uuid));
			commands.insert(commands.end(), num.begin(), num.end());
			src[1] = {"A"};
		}
	}
	else if (!reg_map[src[1]]) {
		auto index = generate_index(src[1], reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());

		src[1] = {"A"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else
		src[1] = {std::string() + reg_map[src[1]]};

	if (dest == src[0])
		if (!is_num(src[1]) || std::stoull(src[1].uuid) > 12)
			commands.push_back(Command({}, Expression{Operator::SUB, dest, src[1]}));
		else {
			auto num = std::stoull(src[1].uuid);
			for (auto i = 0; i < num; ++i)
				commands.push_back(Command({}, Expression{Operator::DEC, dest}));
		}
	else {
		commands.push_back(Command({}, Expression{Operator::COPY, dest, src[0]}));
		if (!is_num(src[1]) || std::stoull(src[1].uuid) > 12)
			commands.push_back(Command({}, Expression{Operator::SUB, dest, src[1]}));
		else {
			auto num = std::stoull(src[1].uuid);
			for (auto i = 0; i < num; ++i)
				commands.push_back(Command({}, Expression{Operator::DEC, dest}));
		}
	}

	if (dest.uuid == "B") {
		set_reg(reg_map, m_dest);
		auto index = generate_index(m_dest, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());

		commands.push_back(Command({}, Expression{Operator::STORE, dest}));
	}

	auto root = std::make_shared<Block>(commands);
	return {root, root};
}

Flow_graph Command::generate_red(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const
{
	std::vector<Command> commands;
	auto dest = m_dest;
	if (!reg_map[dest]) {
		set_reg(reg_map, dest);
		dest = {"B"};
		commands.push_back(Command({}, Expression{Operator::GET, dest}));
		auto index = generate_index(m_dest, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		commands.push_back(Command({}, Expression{Operator::STORE, dest}));
	}
	else {
		dest = {std::string() + reg_map[dest]};
		commands.push_back(Command({}, Expression{Operator::GET, dest}));
	}

	auto root = std::make_shared<Block>(commands);
	return {root, root};
}

Flow_graph Command::generate_wrt(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const
{
	std::vector<Command> commands;
	auto src = m_src.operands[0];
	if (is_num(src)) {
		auto num = generate_num(std::stoull(src.uuid));
		commands.insert(commands.end(), num.begin(), num.end());
		src = {"A"};
	}
	else if (!reg_map[src]) {
		auto index = generate_index(src, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		set_reg(reg_map, src);
		src = {"B"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src}));
	}
	else
		src = {std::string() + reg_map[src]};

	commands.push_back(Command({}, Expression{Operator::PUT, src}));

	auto root = std::make_shared<Block>(commands);
	return {root, root};
}

Flow_graph Command::generate_half(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const
{
	std::vector<Command> commands;
	auto dest = m_dest;
	if (!reg_map[dest])
		dest = {"B"};
	else
		dest = {std::string() + reg_map[dest]};

	auto src = m_src.operands[0];
	if (is_num(src)) {
		auto num = generate_num(std::stoull(src.uuid), dest);
		commands.insert(commands.end(), num.begin(), num.end());
		src = dest;
	}
	else if (!reg_map[src]) {
		auto index = generate_index(src, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		src = dest;
		commands.push_back(Command({}, Expression{Operator::LOAD, src}));
	}
	else
		src = {std::string() + reg_map[src]};

	if (dest == src)
		commands.push_back(Command({}, Expression{Operator::HALF, dest}));
	else {
		commands.push_back(Command({}, Expression{Operator::COPY, dest, src}));
		commands.push_back(Command({}, Expression{Operator::HALF, dest}));
	}

	if (dest.uuid == "B") {
		set_reg(reg_map, m_dest);
		auto index = generate_index(m_dest, reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());

		commands.push_back(Command({}, Expression{Operator::STORE, dest}));
	}

	auto root = std::make_shared<Block>(commands);
	return {root, root};
}

Flow_graph Command::generate(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program) const
{
	auto root = std::make_shared<Block>();
	switch (m_src.op) {
		case Operator::Def:
		case Operator::Und:
		case Operator::Phi:
			return {root, root};
		case Operator::Nul:
			return generate_nul(reg_map, mem_map);
		case Operator::Add:
			return generate_add(reg_map, mem_map);
		case Operator::Sub:
			return generate_sub(reg_map, mem_map);
		case Operator::Red:
			return generate_red(reg_map, mem_map);
		case Operator::Wrt:
			return generate_wrt(reg_map, mem_map);
		case Operator::Half:
			return generate_half(reg_map, mem_map);
		default:
			break;
	}

	std::vector<Command> commands = {*this};

	root = std::make_shared<Block>(commands);
	return {root, root};
}
