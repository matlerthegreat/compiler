#pragma once

#include <set>
#include <vector>
#include <memory>
#include <optional>
#include <functional>

#include "Command.h"
#include "RIG.h"

class Program;

class Block
{
public:
	explicit Block(size_t id = -1);
	explicit Block(std::vector<Command> commands, size_t id = -1);

	void propagate();
	void reduce(std::unordered_set<const Block*>& visited, Program& program);
	void translate(std::unordered_set<const Block*>& visited, std::unordered_map<StrongReference, char> reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	RIG generate_rig() const;
	std::unordered_set<StrongReference> get_inters(std::unordered_set<const Block*>& visited, StrongReference ref, std::vector<Command>::const_reverse_iterator it) const;
	void update_version(std::unordered_set<Reference> input, const std::unordered_set<Reference>& consts);
	void optimize();
	void un_dead();

	std::string to_string() const;
	std::vector<std::string> compile() const;

	std::unordered_set<Reference> get_inputs() const;
	std::unordered_set<Reference> get_outputs() const;

	void merge(std::shared_ptr<Block> other);

	void add_parent(Block* parent);
	void set_condition(Condition condition, std::shared_ptr<Block> block);
	void set_next_block(std::shared_ptr<Block> next_block);
	void set_translated();

private:
	void update_outputs(std::unordered_set<Reference> input);
	void update_inputs(std::unordered_set<Reference> outputs);
	void traverse_const(std::unordered_set<const Block*>& visited, std::function<void(const Block*)> pre_action, std::function<void(const Block*)> post_action) const;
	void traverse(std::unordered_set<const Block*>& visited, std::function<void(Block*)> pre_action, std::function<void(Block*)> post_action);
	void reverse_traverse(std::unordered_set<const Block*>& visited, std::function<void(Block*)> action);
	void reverse_traverse_const(std::unordered_set<const Block*>& visited, std::function<void(const Block*)> action) const;

	void generate_eq(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	void generate_ne(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	void generate_sm(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	void generate_gt(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	void generate_se(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	void generate_ge(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);
	void generate_odd(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program);

	bool m_translated = false;
	const size_t m_id;
	std::vector<Command> m_commands;
	std::shared_ptr<Block> m_next_block;
	std::optional<std::pair<Condition, std::shared_ptr<Block>>> m_conditional_block;
	std::vector<Block*> m_parents;
	std::unordered_set<Reference> m_inputs;
	std::unordered_set<Reference> m_outputs;
	std::unordered_set<Reference> m_undefs;
};

