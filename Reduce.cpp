#include "Command.h"
#include "Program.h"

Flow_graph Command::reduce_mul(Program& program) const
{
	if (is_num(m_src.operands[0])) {
		auto a = std::stoull(m_src.operands[0].uuid);
		auto b = program.get_temporary();
		program.register_var(b.uuid);
		auto result = program.get_temporary();
		program.register_var(result.uuid);

		std::vector<Command> commands;
		commands.push_back(Command(b, Expression{Operator::Def}));
		commands.push_back(Command(result, Expression{Operator::Def}));

		commands.push_back(Command(b, Expression{Operator::Nul, m_src.operands[1]}));
		commands.push_back(Command(result, Expression{Operator::Nul, {"0"}}));
		while(a != 0)
		{
			if(a & 1) // If the lowest order bit is set in A?
			{
				commands.push_back(Command(result, Expression{Operator::Add, result, b}));
			}
			a = a >> 1; // Note: This must set highest order bit ZERO. It must not copy the sign bit.
			commands.push_back(Command(b, Expression{Operator::Add, b, b}));
		}

		commands.push_back(Command(m_dest, Expression{Operator::Nul, result}));
		commands.push_back(Command(b, Expression{Operator::Und}));
		commands.push_back(Command(result, Expression{Operator::Und}));

		auto root = std::make_shared<Block>(commands);
		return {root, root};
	}
	auto a = program.get_temporary();
	program.register_var(a.uuid);
	auto b = program.get_temporary();
	program.register_var(b.uuid);
	auto result = program.get_temporary();
	program.register_var(result.uuid);
	std::array<Reference, 2> src;

	std::vector<Command> commands;
	commands.push_back(Command(a, Expression{Operator::Def}));
	commands.push_back(Command(b, Expression{Operator::Def}));
	commands.push_back(Command(result, Expression{Operator::Def}));

	commands.push_back(Command(a, Expression{Operator::Nul, m_src.operands[0]}));
	commands.push_back(Command(b, Expression{Operator::Nul, m_src.operands[1]}));
	commands.push_back(Command(result, Expression{Operator::Nul, {"0"}}));

	auto root = std::make_shared<Block>(commands);

	commands = {};
	commands.push_back(Command(a, Expression{Operator::Phi, a, a}));
	commands.push_back(Command(b, Expression{Operator::Phi, b, b}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));

	auto loop_header = std::make_shared<Block>(commands, program.register_block());
	root->set_next_block(loop_header);
	loop_header->add_parent(root.get());

	auto if_block = std::make_shared<Block>(program.register_block());
	src[0] = a; src[0].version = 1;
	src[1] = {"0"};
	Condition cond = {src, Relation::Ne};
	loop_header->set_condition(cond, if_block);
	if_block->add_parent(loop_header.get());

	commands = {};
	commands.push_back(Command(result, Expression{Operator::Add, result, b}));

	auto odd_block = std::make_shared<Block>(commands, program.register_block());
	cond = {a, a, Relation::Odd};
	if_block->set_condition(cond, odd_block);
	odd_block->add_parent(if_block.get());

	commands = {};
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(a, Expression{Operator::Half, a}));
	commands.push_back(Command(b, Expression{Operator::Add, b, b}));

	auto loop_footer = std::make_shared<Block>(commands, program.register_block());
	if_block->set_next_block(loop_footer);
	loop_footer->add_parent(if_block.get());
	odd_block->set_next_block(loop_footer);
	loop_footer->add_parent(odd_block.get());

	loop_footer->set_next_block(loop_header);
	loop_header->add_parent(loop_footer.get());

	commands = {};
	commands.push_back(Command(m_dest, Expression{Operator::Nul, result}));
	commands.push_back(Command(a, Expression{Operator::Und}));
	commands.push_back(Command(b, Expression{Operator::Und}));
	commands.push_back(Command(result, Expression{Operator::Und}));

	auto leaf = std::make_shared<Block>(commands, program.register_block());
	loop_header->set_next_block(leaf);
	leaf->add_parent(loop_header.get());

	return {root, leaf};
}

Flow_graph Command::reduce_div(Program& program) const
{
	auto src = m_src.operands;
	if (is_num(src[1])) {
		auto num = std::stoull(src[1].uuid);
		if (num == 0) {
			std::vector<Command> commands;
			commands.push_back(Command(m_dest, Expression{Operator::Nul, {"0"}}));

			auto root = std::make_shared<Block>(commands);
			return {root, root};
		}
		if (num == 1) {
			std::vector<Command> commands;
			commands.push_back(Command(m_dest, Expression{Operator::Nul, src[0]}));

			auto root = std::make_shared<Block>(commands);
			return {root, root};
		}

		auto halves = 0;
		for (auto i = 1; i < 64; ++i) {
			if (num == (1u << i)) {
				halves = i;
				break;
			}
		}

		if (halves) {
			std::vector<Command> commands;
			commands.push_back(Command(m_dest, Expression{Operator::Half, src[0]}));

			for (auto i = 1; i < halves; ++i) {
				commands.push_back(Command(m_dest, Expression{Operator::Half, m_dest}));
			}

			auto root = std::make_shared<Block>(commands);
			return {root, root};
		}
	}

	auto scaled_divisor = program.get_temporary();
	program.register_var(scaled_divisor.uuid);
	auto remain = program.get_temporary();
	program.register_var(remain.uuid);
	auto result = program.get_temporary();
	program.register_var(result.uuid);
	auto multiple = program.get_temporary();
	program.register_var(multiple.uuid);

	std::vector<Command> commands;
	commands.push_back(Command(scaled_divisor, Expression{Operator::Def}));
	commands.push_back(Command(remain, Expression{Operator::Def}));
	commands.push_back(Command(result, Expression{Operator::Def}));
	commands.push_back(Command(multiple, Expression{Operator::Def}));
	commands.push_back(Command(result, Expression{Operator::Nul, {"0"}}));
	auto root = std::make_shared<Block>(commands);

    commands = {}; 
    auto zero_block = std::make_shared<Block>(commands, program.register_block());
	root->set_next_block(zero_block);
	zero_block->add_parent(root.get());

	commands = {};
	commands.push_back(Command(scaled_divisor, Expression{Operator::Nul, src[1]}));
	commands.push_back(Command(remain, Expression{Operator::Nul, src[0]}));
	commands.push_back(Command(multiple, Expression{Operator::Nul, {"1"}}));
    auto init_block = std::make_shared<Block>(commands, program.register_block());
    zero_block->set_next_block(init_block);
    init_block->add_parent(zero_block.get());

    commands = {}; 
	commands.push_back(Command(scaled_divisor, Expression{Operator::Phi, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Phi, multiple, multiple}));
    auto reductor_header = std::make_shared<Block>(commands, program.register_block());
    init_block->set_next_block(reductor_header);
    reductor_header->add_parent(init_block.get());

    commands = {}; 
	commands.push_back(Command(scaled_divisor, Expression{Operator::Add, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Add, multiple, multiple}));
    auto reductor = std::make_shared<Block>(commands, program.register_block());
    Condition cond = {scaled_divisor, src[0], Relation::Sm};
    reductor_header->set_condition(cond, reductor);
    reductor->add_parent(reductor_header.get());
    reductor->set_next_block(reductor_header);
    reductor_header->add_parent(reductor.get());

    commands = {}; 
	commands.push_back(Command(remain, Expression{Operator::Phi, remain, remain}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(scaled_divisor, Expression{Operator::Phi, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Phi, multiple, multiple}));
    auto loop_header = std::make_shared<Block>(commands, program.register_block());
	reductor_header->set_next_block(loop_header);
	loop_header->add_parent(reductor_header.get());

    commands = {}; 
	commands.push_back(Command(remain, Expression{Operator::Sub, remain, scaled_divisor}));
	commands.push_back(Command(result, Expression{Operator::Add, result, multiple}));
    auto if_block = std::make_shared<Block>(commands, program.register_block());
	cond = {remain, scaled_divisor, Relation::Ge};
	loop_header->set_condition(cond, if_block);
	if_block->add_parent(loop_header.get());

    commands = {}; 
	commands.push_back(Command(remain, Expression{Operator::Phi, remain, remain}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(scaled_divisor, Expression{Operator::Half, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Half, multiple}));
    auto loop_body = std::make_shared<Block>(commands, program.register_block());
	loop_header->set_next_block(loop_body);
	loop_body->add_parent(loop_header.get());
	if_block->set_next_block(loop_body);
	loop_body->add_parent(if_block.get());

	cond = {multiple, Reference{"0"}, Relation::Gt};
	loop_body->set_condition(cond, loop_header);
	loop_header->add_parent(loop_body.get());

    commands = {}; 
	commands.push_back(Command(scaled_divisor, Expression{Operator::Phi, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(remain, Expression{Operator::Phi, remain, remain}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(multiple, Expression{Operator::Phi, multiple, multiple}));
	commands.push_back(Command(m_dest, Expression{Operator::Nul, result}));
	commands.push_back(Command(scaled_divisor, Expression{Operator::Und}));
	commands.push_back(Command(remain, Expression{Operator::Und}));
	commands.push_back(Command(result, Expression{Operator::Und}));
	commands.push_back(Command(multiple, Expression{Operator::Und}));
    auto leaf = std::make_shared<Block>(commands, program.register_block());
	cond = {src[1], Reference{"0"}, Relation::Se};
	zero_block->set_condition(cond, leaf);
	leaf->add_parent(zero_block.get());
	loop_body->set_next_block(leaf);
	leaf->add_parent(loop_body.get());

	return {root, leaf};
}

Flow_graph Command::reduce_mod(Program& program) const
{
	auto scaled_divisor = program.get_temporary();
	program.register_var(scaled_divisor.uuid);
	auto remain = program.get_temporary();
	program.register_var(remain.uuid);
	auto result = program.get_temporary();
	program.register_var(result.uuid);
	auto multiple = program.get_temporary();
	program.register_var(multiple.uuid);

	auto src = m_src.operands;
	std::vector<Command> commands;
	commands.push_back(Command(scaled_divisor, Expression{Operator::Def}));
	commands.push_back(Command(remain, Expression{Operator::Def}));
	commands.push_back(Command(result, Expression{Operator::Def}));
	commands.push_back(Command(multiple, Expression{Operator::Def}));
	commands.push_back(Command(remain, Expression{Operator::Nul, {"0"}}));
	auto root = std::make_shared<Block>(commands);

    commands = {}; 
    auto zero_block = std::make_shared<Block>(commands, program.register_block());
	root->set_next_block(zero_block);
	zero_block->add_parent(root.get());

	commands = {};
	commands.push_back(Command(scaled_divisor, Expression{Operator::Nul, src[1]}));
	commands.push_back(Command(remain, Expression{Operator::Nul, src[0]}));
	commands.push_back(Command(multiple, Expression{Operator::Nul, {"1"}}));
	commands.push_back(Command(result, Expression{Operator::Nul, {"0"}}));
    auto init_block = std::make_shared<Block>(commands, program.register_block());
    zero_block->set_next_block(init_block);
    init_block->add_parent(zero_block.get());

    commands = {}; 
	commands.push_back(Command(scaled_divisor, Expression{Operator::Phi, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Phi, multiple, multiple}));
    auto reductor_header = std::make_shared<Block>(commands, program.register_block());
    init_block->set_next_block(reductor_header);
    reductor_header->add_parent(init_block.get());

    commands = {}; 
	commands.push_back(Command(scaled_divisor, Expression{Operator::Add, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Add, multiple, multiple}));
    auto reductor = std::make_shared<Block>(commands, program.register_block());
    Condition cond = {scaled_divisor, src[0], Relation::Sm};
    reductor_header->set_condition(cond, reductor);
    reductor->add_parent(reductor_header.get());
    reductor->set_next_block(reductor_header);
    reductor_header->add_parent(reductor.get());

    commands = {}; 
	commands.push_back(Command(remain, Expression{Operator::Phi, remain, remain}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(scaled_divisor, Expression{Operator::Phi, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Phi, multiple, multiple}));
    auto loop_header = std::make_shared<Block>(commands, program.register_block());
	reductor_header->set_next_block(loop_header);
	loop_header->add_parent(reductor_header.get());

    commands = {}; 
	commands.push_back(Command(remain, Expression{Operator::Sub, remain, scaled_divisor}));
	commands.push_back(Command(result, Expression{Operator::Add, result, multiple}));
    auto if_block = std::make_shared<Block>(commands, program.register_block());
	cond = {remain, scaled_divisor, Relation::Ge};
	loop_header->set_condition(cond, if_block);
	if_block->add_parent(loop_header.get());

    commands = {}; 
	commands.push_back(Command(remain, Expression{Operator::Phi, remain, remain}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(scaled_divisor, Expression{Operator::Half, scaled_divisor}));
	commands.push_back(Command(multiple, Expression{Operator::Half, multiple}));
    auto loop_body = std::make_shared<Block>(commands, program.register_block());
	loop_header->set_next_block(loop_body);
	loop_body->add_parent(loop_header.get());
	if_block->set_next_block(loop_body);
	loop_body->add_parent(if_block.get());

	cond = {multiple, Reference{"0"}, Relation::Gt};
	loop_body->set_condition(cond, loop_header);
	loop_header->add_parent(loop_body.get());

    commands = {}; 
	commands.push_back(Command(scaled_divisor, Expression{Operator::Phi, scaled_divisor, scaled_divisor}));
	commands.push_back(Command(remain, Expression{Operator::Phi, remain, remain}));
	commands.push_back(Command(result, Expression{Operator::Phi, result, result}));
	commands.push_back(Command(multiple, Expression{Operator::Phi, multiple, multiple}));
	commands.push_back(Command(m_dest, Expression{Operator::Nul, remain}));
	commands.push_back(Command(scaled_divisor, Expression{Operator::Und}));
	commands.push_back(Command(remain, Expression{Operator::Und}));
	commands.push_back(Command(result, Expression{Operator::Und}));
	commands.push_back(Command(multiple, Expression{Operator::Und}));
    auto leaf = std::make_shared<Block>(commands, program.register_block());
	cond = {src[1], Reference{"0"}, Relation::Se};
	zero_block->set_condition(cond, leaf);
	leaf->add_parent(zero_block.get());
	loop_body->set_next_block(leaf);
	leaf->add_parent(loop_body.get());

	return {root, leaf};
}

Flow_graph Command::reduce(Program& program) const
{
	switch (m_src.op) {
		case Operator::Mul:
			return reduce_mul(program);
		case Operator::Div:
			return reduce_div(program);
		case Operator::Mod:
			return reduce_mod(program);
		default:
			break;
	}

	std::vector<Command> commands = {*this};
	auto root = std::make_shared<Block>(commands);
	return {root, root};
}
