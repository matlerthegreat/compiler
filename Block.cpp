#include "Block.h"
#include "Program.h"

#include <algorithm>

Block::Block(size_t id) :
	m_id(id)
{}

Block::Block(std::vector<Command> commands, size_t id) :
	m_commands(commands),
	m_id(id)
{
	for (const auto& command: m_commands) {
		auto outputs = command.get_outputs();
		m_outputs.insert(outputs.begin(), outputs.end());
		auto undefs = command.get_undefs();
		m_undefs.insert(undefs.begin(), undefs.end());
	}

	for (auto undef: m_undefs)
		m_outputs.erase(undef);
}

void Block::propagate()
{
	auto action = [](Block* block) {
		std::optional<std::array<StrongReference, 2>> replacement;
		auto replace = [&replacement](Block* block) {
			if (block->m_conditional_block) {
				auto& src = block->m_conditional_block->first.operands;
				auto& propagated = block->m_conditional_block->first.propagated;
				if (is_same(src[0], replacement.value()[0]))
					propagated[0] = replacement.value()[1];
				else if (is_same(src[1], replacement.value()[0]))
					propagated[1] = replacement.value()[1];

				if (is_num(propagated[0]) && is_num(propagated[1])) {
					auto a = std::stoull(propagated[0].uuid);
					auto b = std::stoull(propagated[1].uuid);
					bool result;
					switch (block->m_conditional_block->first.rel) {
						case Relation::Eq:
							result = a == b;
							break;
						case Relation::Ne:
							result = a != b;
							break;
						case Relation::Sm:
							result = a < b;
							break;
						case Relation::Gt:
							result = a > b;
							break;
						case Relation::Se:
							result = a <= b;
							break;
						case Relation::Ge:
							result = a >= b;
							break;
						case Relation::Odd:
							result = a & 1;
							break;
					}

					if (result) {
						auto found = std::find(block->m_next_block->m_parents.begin(), block->m_next_block->m_parents.end(), block);
						block->m_next_block->m_parents.erase(found);
						block->m_next_block = block->m_conditional_block->second;
						block->m_conditional_block.reset();
					}
					else {
						auto found = std::find(block->m_conditional_block->second->m_parents.begin(), block->m_conditional_block->second->m_parents.end(), block);
						block->m_conditional_block->second->m_parents.erase(found);
						block->m_conditional_block.reset();
					}
				}
			}
			auto& commands = block->m_commands;
			for (auto& command: commands) {
				command.replace(*replacement);
			}
		};

		auto& commands = block->m_commands;
		for (auto& command: commands) {
			replacement = command.get_replacement();
			if (replacement) {
				std::unordered_set<const Block*> visited;
				block->traverse(visited, replace, [](Block*){});
			}
		}
	};

	std::unordered_set<const Block*> visited;
	traverse(visited, action, [](Block*){});
}

void Block::reduce(std::unordered_set<const Block*>& visited, Program& program)
{
	auto inserted = visited.insert(this);
	if (!inserted.second)
		return;

	auto it = m_commands.begin();
	while (it != m_commands.end()) {
		auto graph = it->reduce(program);
		if (graph.root != graph.leaf) {
			auto commands = std::vector<Command>(std::next(it), m_commands.end());
			m_commands.erase(it, m_commands.end());
			auto leaf = std::make_shared<Block>(commands);
			if (m_conditional_block) {
				leaf->set_condition(m_conditional_block->first, m_conditional_block->second);
				auto& parents = m_conditional_block->second->m_parents;
				for (auto it = parents.begin(); it != parents.end(); ++it)
					if (*it == this) {
						parents.erase(it);
						break;
					}
				m_conditional_block->second->add_parent(leaf.get());
			}
			if (m_next_block) {
				leaf->m_next_block = m_next_block;
				auto& parents = m_next_block->m_parents;
				for (auto it = parents.begin(); it != parents.end(); ++it)
					if (*it == this) {
						parents.erase(it);
						break;
					}
				m_next_block->add_parent(leaf.get());
			}

			merge(graph.root);

			graph.leaf->merge(leaf);

			break;
		}
		else {
			it = m_commands.erase(it);
			it = m_commands.insert(it, graph.root->m_commands.begin(), graph.root->m_commands.end());
			it += graph.root->m_commands.size();
		}
	}

	if (m_conditional_block) {
		m_conditional_block->second->reduce(visited, program);
	}

	if (m_next_block)
		m_next_block->reduce(visited, program);
}

void Block::translate(std::unordered_set<const Block*>& visited, std::unordered_map<StrongReference, char> reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto inserted = visited.insert(this);
	if (!inserted.second)
		return;

	auto original_reg_map = reg_map;

	auto it = m_commands.begin();
	while (it != m_commands.end()) {
		auto graph = it->generate(reg_map, mem_map, program);
		if (graph.root != graph.leaf) {
			auto commands = std::vector<Command>(std::next(it), m_commands.end());
			m_commands.erase(it, m_commands.end());
			auto leaf = std::make_shared<Block>(commands);
			if (m_conditional_block) {
				leaf->set_condition(m_conditional_block->first, m_conditional_block->second);
				auto& parents = m_conditional_block->second->m_parents;
				for (auto it = parents.begin(); it != parents.end(); ++it)
					if (*it == this) {
						parents.erase(it);
						break;
					}
				m_conditional_block->second->add_parent(leaf.get());
			}
			if (m_next_block) {
				leaf->m_next_block = m_next_block;
				auto& parents = m_next_block->m_parents;
				for (auto it = parents.begin(); it != parents.end(); ++it)
					if (*it == this) {
						parents.erase(it);
						break;
					}
				m_next_block->add_parent(leaf.get());
			}

			merge(graph.root);

			graph.leaf->merge(leaf);

			break;
		}
		else {
			it = m_commands.erase(it);
			it = m_commands.insert(it, graph.root->m_commands.begin(), graph.root->m_commands.end());
			it += graph.root->m_commands.size();
		}
	}

	if (m_conditional_block) {
		if (!m_translated) {
			switch (m_conditional_block->first.rel) {
				case Relation::Eq:
					generate_eq(reg_map, mem_map, program);
					break;
				case Relation::Ne:
					generate_ne(reg_map, mem_map, program);
					break;
				case Relation::Sm:
					generate_sm(reg_map, mem_map, program);
					break;
				case Relation::Gt:
					generate_gt(reg_map, mem_map, program);
					break;
				case Relation::Se:
					generate_se(reg_map, mem_map, program);
					break;
				case Relation::Ge:
					generate_ge(reg_map, mem_map, program);
					break;
				case Relation::Odd:
					generate_odd(reg_map, mem_map, program);
					break;
			}
		}
		m_conditional_block->second->translate(visited, original_reg_map, mem_map, program);
	}

	if (m_next_block) {
		m_next_block->translate(visited, original_reg_map, mem_map, program);
	}
}

RIG Block::generate_rig() const
{
	RIG rig;
	auto first_scan = [&rig](const Block* block) {
		const auto& commands = block->m_commands;
		for (auto it = commands.begin(); it != commands.end(); ++it) {
			for (auto def: it->get_strong_outputs()) {
				rig.add_ref(def);
			}
		}
	};
	std::unordered_set<const Block*> visited;
	traverse_const(visited, first_scan, [](const Block*){});

	auto action = [&rig](const Block* block) {
		const auto& commands = block->m_commands;
		for (auto it = commands.rbegin(); it != commands.rend(); ++it) {
			bool has_offset = false;
			for (auto def: it->get_strong_outputs()) {
				if (def.offset)
					has_offset = true;

				auto conns = it->get_connections();
				for (auto conn: conns)
					rig.add_conect(def, conn);
			}

			for (auto src: it->get_strong_inputs()) {
				std::unordered_set<const Block*> visited;
				auto iters = block->get_inters(visited, src, std::next(it));
				for (auto iter: iters) {
					rig.add_inter(src, iter);
				}
				if (src.offset || has_offset) {
					for (auto dest: it->get_strong_outputs())
						rig.add_inter(src, dest);
				}
			}
		}
		if (block->m_conditional_block) {
			auto cond = block->m_conditional_block->first;
			for (auto src: cond.operands) {
				std::unordered_set<const Block*> visited;
				auto iters = block->get_inters(visited, src, commands.rbegin());
				for (auto iter: iters) {
					rig.add_inter(src, iter);
				}
			}
		}
	};


	const Block* leaf;
	visited = {};
	auto find_leaf = [&leaf] (const Block* block) {
		if (!block->m_conditional_block && !block->m_next_block)
			leaf = block;
	};
	traverse_const(visited, find_leaf, [](const Block*){});
	visited = {};
	leaf->reverse_traverse_const(visited, action);

	return rig;
}

std::unordered_set<StrongReference> Block::get_inters(std::unordered_set<const Block*>& visited, StrongReference ref, std::vector<Command>::const_reverse_iterator it) const
{
	std::unordered_set<StrongReference> result;

	for (; it != m_commands.rend(); ++it) {
		const auto& outputs = it->get_strong_outputs();
		if (outputs.count(ref) > 0) {
			break;
		}
		result.insert(outputs.begin(), outputs.end());
	}

	auto inserted = visited.insert(this);
	if (!inserted.second)
		return result;

	if (it != m_commands.rend())
		return result;

	bool p = false;
	for (const auto& parent: m_parents) {
		auto found = parent->m_outputs.find(ref);
		if (found == parent->m_outputs.end() || found->version != ref.version)
			continue;
		auto next = parent->get_inters(visited, ref, parent->m_commands.rbegin());
		result.insert(next.begin(), next.end());
		p = true;
	}

	if (p)
		return result;
	else
		return {};
}

void Block::update_version(std::unordered_set<Reference> defs, const std::unordered_set<Reference>& consts)
{
	/*auto reset = [](Block* block) {
		auto outputs = block->m_outputs;
		block->m_outputs.clear();
		for (auto out: outputs) {
			out.version = -1;
			block->m_outputs.insert(out);
		}
	};
	std::unordered_set<const Block*> visited;
	traverse(visited, reset, [](const Block*){});*/

	auto pre_action = [&defs, &consts](Block* block) {
		/*
		auto& commands = block->m_commands;

		for (auto parent: block->m_parents) {
			auto inputs = parent->get_outputs();
			for (auto input: inputs) {
				auto inserted = block->m_outputs.insert(input);
				if (!inserted.second && !inserted.first->offset) {
					auto expr = Expression{Operator::Phi, input, input};

					auto command = Command(input, expr);
					commands.insert(commands.begin(), command);
				}
			}
		}*/
		block->m_outputs = {};
		for (auto parent: block->m_parents) {
			auto inputs = parent->get_outputs();
			for (auto& input : inputs) {
				auto inserted = block->m_outputs.insert(input);
				if (!inserted.second && inserted.first->version == -1) {
					auto node = block->m_outputs.extract(inserted.first);
					node.value().version = input.version;
					block->m_outputs.insert(std::move(node));
				}
			}
		}

		for(auto& command: block->m_commands) {
			command.update_dest(defs, block->m_outputs, consts);
		}
	};

	auto post_action = [](Block* block) {
		std::unordered_set<Reference> inputs = {};
		for (auto parent: block->m_parents) {
			auto input = parent->get_outputs();
			inputs.insert(input.begin(), input.end());
		}

		for(auto& command: block->m_commands) {
			command.update_src(inputs);
		}

		if (block->m_conditional_block) {
			auto& cond = block->m_conditional_block->first;
			for (auto& ref: cond.operands) {
				if (is_num(ref))
					continue;

				auto offset = ref.offset;
				auto found = inputs.find(ref);
				if (found == inputs.end())
					throw std::runtime_error("\"" + ref.uuid + "\" not declared");
				ref = *found;
				if (offset && !is_num(*offset)) {
					auto found = inputs.find(*offset);
					if (found == inputs.end())
						throw std::runtime_error("\"" + offset->uuid + "\" not declared");
					offset = std::make_shared<Reference>(*found);
				}
				ref.offset = offset;
			}
		}
	};

	std::unordered_set<const Block*> visited;
	traverse(visited, pre_action, [](const Block*){});
	visited = {};
	traverse(visited, post_action, [](const Block*){});

	auto phi = [](Block* block) {
		std::vector<std::unordered_set<Reference>> defs;
		for (auto parent: block->m_parents)
			defs.push_back(parent->get_outputs());

		auto& commands = block->m_commands;
		for (auto& command: block->m_commands) {
			if (!command.update_phi(defs))
				return;
		}
	};
	visited = {};
	traverse(visited, phi, [](const Block*){});
}

void Block::un_dead()
{
	bool changed = false;
	auto mark = [&changed](Block* block) {
		if (block->m_conditional_block) {
			auto operands = block->m_conditional_block->first.operands;
			for (auto req: operands) {
				auto sub_mark = [req](Block* block) {
					auto& commands = block->m_commands;
					for (auto it = commands.rbegin(); it != commands.rend(); ++it)
						it->check_requirement(req);
				};

				std::unordered_set<const Block*> visited;
				block->reverse_traverse(visited, sub_mark);
			}
		}

		if (block->m_conditional_block) {
		}
		auto& commands = block->m_commands;
		for (auto it = commands.rbegin(); it != commands.rend(); ++it) {
			auto reqs = it->get_requrements();
			for (auto req: reqs) {
				auto sub_mark = [&changed, req](Block* block) {
					auto& commands = block->m_commands;
					for (auto it = commands.rbegin(); it != commands.rend(); ++it)
						if (it->check_requirement(req))
							changed = true;
				};

				std::unordered_set<const Block*> visited;
				block->reverse_traverse(visited, sub_mark);
			}
		}
	};


	Block* leaf;
	std::unordered_set<const Block*> visited;
	auto find_leaf = [&leaf] (Block* block) {
		if (!block->m_conditional_block && !block->m_next_block)
			leaf = block;
	};
	traverse(visited, find_leaf, [](const Block*){});
	do {
		changed = false;
		visited = {};
		leaf->reverse_traverse(visited, mark);
	} while (changed);

	auto remove = [](Block* block) {
		auto& commands = block->m_commands;
		for (auto it = commands.begin(); it != commands.end(); ++it) {
			if (!it->is_used()) {
				it = std::prev(commands.erase(it));
			}
		}
	};

	visited = {};
	traverse(visited, remove, [](const Block*){});
}

void Block::optimize()
{
	auto first_pass = [](Block* block)
	{
		auto& commands = block->m_commands;
		auto load = std::find_if(commands.rbegin(), commands.rend(), [](const auto& command){
					return command.get_operator() == Operator::LOAD;
				});
		while (load != commands.rend()) {
			auto store = std::find_if(load, commands.rend(), [](const auto& command){
					return command.get_operator() == Operator::STORE;
				});

			if (store != commands.rend() && load->is_complementary(*store)) {
				auto it1 = std::next(load);
				auto it2 = store;
				bool is_complementary = true;

				while (it1->is_a_reset()) {
					std::advance(it2, 1);

					if (it2 == commands.rend() || !it1->is_complementary(*it2)) {
						is_complementary = false;
						break;
					}

					std::advance(it1, 1);
				}

				if (is_complementary) {
					std::advance(it2, 1);
					commands.erase(it2.base(), load.base());
					load = std::find_if(it2, commands.rend(), [](const auto& command){
							return command.get_operator() == Operator::LOAD;
							});
				}
				else {
					load = std::find_if(std::next(load), commands.rend(), [](const auto& command){
							return command.get_operator() == Operator::LOAD;
							});
				}
			}
			else
				load = commands.rend();
		}
	};

	std::unordered_set<const Block*> visited;
	traverse(visited, first_pass, [](const Block*){});
}

std::string Block::to_string() const
{
	std::string result;
	std::unordered_set<const Block*> visited;
	auto action = [&result, &visited](const Block* block)
	{
		result += "##### Block: " + std::to_string(block->m_id) + " (";
		for (auto& dep: block->m_outputs)
			result += std::to_string(dep) + ", ";
		result += ") #####\n";

		for (const auto command : block->m_commands) {
			result += command.to_string() + '\n';
		}

		if (block->m_conditional_block) {
			result += "if " + std::to_string(block->m_conditional_block->first);
			result += " goto " + std::to_string(block->m_conditional_block->second->m_id) + '\n';
		}

		if (block->m_next_block) {
			auto found = visited.find(block->m_next_block.get());
			if (found != visited.end())
				result += "goto " + std::to_string(block->m_next_block->m_id) + '\n';
		}
		else {
			result += "HALT\n";
		}
	};

	traverse_const(visited, action, [](const Block*){});

	return result;
}

std::vector<std::string> Block::compile() const
{
	std::unordered_set<const Block*> visited;
	std::unordered_map<size_t, size_t> label_map;
	size_t command_count = 0;
	auto label_scan = [&command_count, &visited, &label_map](const Block* block)
	{
		label_map[block->m_id] = command_count;

		command_count += block->m_commands.size();

		if (block->m_conditional_block) {
			++command_count;
		}

		if (block->m_next_block) {
			auto found = visited.find(block->m_next_block.get());
			if (found != visited.end())
				++command_count;
		}
		else
			++command_count;
	};

	traverse_const(visited, label_scan, [](const Block*){});

	visited = {};
	std::vector<std::string> result;
	auto compile = [&result, &visited, &label_map](const Block* block)
	{
		for (const auto command : block->m_commands) {
			result.push_back(command.compile());
		}

		if (block->m_conditional_block) {
			auto id = block->m_conditional_block->second->m_id;
			auto cond = block->m_conditional_block->first;
			if (cond.rel == Relation::Odd)
				result.push_back("JODD " + cond.operands[0].uuid + " " + std::to_string(label_map[id]));
			else
				result.push_back("JZERO A " + std::to_string(label_map[id]));
		}

		if (block->m_next_block) {
			auto found = visited.find(block->m_next_block.get());
			if (found != visited.end()) {
				auto id = block->m_next_block->m_id;
				result.push_back("JUMP " + std::to_string(label_map[id]));
			}
		}
		else {
			result.push_back("HALT");
		}
	};

	traverse_const(visited, compile, [](const Block*){});

	return result;
}

std::unordered_set<Reference> Block::get_outputs()const
{
	return m_outputs;
}

void Block::merge(std::shared_ptr<Block> other)
{
	for (auto& command: other->m_commands) {
		m_commands.push_back(command);
	}

	if (other->m_next_block) {
		auto& parents = other->m_next_block->m_parents;
		auto t = std::find(parents.begin(), parents.end(), other.get());
		if (t != parents.end()) *t = this;
	}
	m_next_block = other->m_next_block;
	if (other->m_conditional_block) {
		auto& parents = other->m_conditional_block->second->m_parents;
		*std::find(parents.begin(), parents.end(), other.get()) = this;
	}
	m_conditional_block = other->m_conditional_block;

	m_translated = other->m_translated;

	other->m_undefs.insert(m_undefs.begin(), m_undefs.end());
	m_undefs = other->m_undefs;

	other->m_outputs.insert(m_outputs.begin(), m_outputs.end());
	m_outputs = other->m_outputs;

	for (auto undef: m_undefs)
		m_outputs.erase(undef);

	if (m_conditional_block)
		m_conditional_block->second->update_outputs(m_outputs);

	if (m_next_block)
		m_next_block->update_outputs(m_outputs);
}

void Block::add_parent(Block* parent)
{
	m_parents.push_back(parent);
	auto inputs = parent->get_outputs();
	update_outputs(inputs);
}

void Block::set_condition(Condition condition, std::shared_ptr<Block> block)
{
	m_conditional_block = std::make_pair(condition, block);
	block->update_outputs(m_outputs);
}

void Block::set_next_block(std::shared_ptr<Block> next_block)
{
	m_next_block = next_block;
	next_block->update_outputs(m_outputs);
}

void Block::update_outputs(std::unordered_set<Reference> inputs)
{
	for (auto undef: m_undefs)
		inputs.erase(undef);

	bool updated = false;
	for (auto input: inputs) {
		auto inserted = m_outputs.insert(input);
		if (inserted.second)
			updated = true;
	}

	if (updated) {
		if (m_conditional_block)
			m_conditional_block->second->update_outputs(m_outputs);

		if (m_next_block)
			m_next_block->update_outputs(m_outputs);
	}
}

void Block::traverse_const(std::unordered_set<const Block*>& visited, std::function<void(const Block*)> pre_action, std::function<void(const Block*)> post_action) const
{
	auto inserted = visited.insert(this);
	if (!inserted.second)
		return;

	pre_action(this);

	if (m_next_block)
		m_next_block->traverse_const(visited, pre_action, post_action);

	if (m_conditional_block)
		m_conditional_block->second->traverse_const(visited, pre_action, post_action);

	post_action(this);
}

void Block::traverse(std::unordered_set<const Block*>& visited, std::function<void(Block*)> pre_action, std::function<void(Block*)> post_action)
{
	auto inserted = visited.insert(this);
	if (!inserted.second)
		return;

	pre_action(this);

	if (m_next_block)
		m_next_block->traverse(visited, pre_action, post_action);

	if (m_conditional_block)
		m_conditional_block->second->traverse(visited, pre_action, post_action);

	post_action(this);
}

void Block::reverse_traverse(std::unordered_set<const Block*>& visited, std::function<void(Block*)> action)
{
	auto inserted = visited.insert(this);
	if (!inserted.second)
		return;

	action(this);

	for (auto& parent: m_parents)
		parent->reverse_traverse(visited, action);
}

void Block::reverse_traverse_const(std::unordered_set<const Block*>& visited, std::function<void(const Block*)> action) const
{
	auto inserted = visited.insert(this);
	if (!inserted.second)
		return;

	action(this);

	for (const auto& parent: m_parents)
		parent->reverse_traverse_const(visited, action);
}

void Block::set_translated()
{
	m_translated = true;
}

void Block::generate_ne(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	std::swap(m_next_block, m_conditional_block->second);
	generate_eq(reg_map, mem_map, program);
}

void Block::generate_eq(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto src = m_conditional_block->first.operands;
	if (is_num(src[0])) {
		set_reg(reg_map, src[0]);
		auto num = generate_num(std::stoull(src[0].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[0] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::COPY, src[0], {"A"}}));
	}
	else if (!reg_map[src[0]]) {
		set_reg(reg_map, src[0]);
		auto index = generate_index(src[0], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[0] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else
		src[0] = {std::string() + reg_map[src[0]]};

	if (is_num(src[1])) {
		auto num = generate_num(std::stoull(src[1].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[1] = {"A"};
	}
	else if (!reg_map[src[1]]) {
		auto index = generate_index(src[1], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[1] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else {
		src[1] = {std::string() + reg_map[src[1]]};
		m_commands.push_back(Command({}, Expression{Operator::COPY, Reference{"A"}, src[1]}));
		src[1] = {"A"};
	}

	m_commands.push_back(Command({}, Expression{Operator::SUB, src[1], src[0]}));

	std::vector<Command> commands;

	src = m_conditional_block->first.operands;
	if (is_num(src[1])) {
		set_reg(reg_map, src[1]);
		auto num = generate_num(std::stoull(src[1].uuid));
		commands.insert(commands.end(), num.begin(), num.end());
		src[1] = {"B"};
		commands.push_back(Command({}, Expression{Operator::COPY, src[1], {"A"}}));
	}
	else if (!reg_map[src[1]]) {
		set_reg(reg_map, src[1]);
		auto index = generate_index(src[1], reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		src[1] = {"B"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else
		src[1] = {std::string() + reg_map[src[1]]};

	if (is_num(src[0])) {
		auto num = generate_num(std::stoull(src[0].uuid));
		commands.insert(commands.end(), num.begin(), num.end());
		src[0] = {"A"};
	}
	else if (!reg_map[src[0]]) {
		auto index = generate_index(src[0], reg_map, mem_map);
		commands.insert(commands.end(), index.begin(), index.end());
		src[0] = {"A"};
		commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else {
		src[0] = {std::string() + reg_map[src[0]]};
		commands.push_back(Command({}, Expression{Operator::COPY, Reference{"A"}, src[0]}));
		src[0] = {"A"};
	}

	commands.push_back(Command({}, Expression{Operator::SUB, src[0], src[1]}));
	auto other = std::make_shared<Block>(commands, program.register_block());


	other->set_next_block(m_next_block);
	m_next_block->add_parent(other.get());

	Condition cond = {m_conditional_block->first.operands, Relation::Ge};
	other->set_condition(cond, m_conditional_block->second);
	m_conditional_block->second->add_parent(other.get());

	cond = {m_conditional_block->first.operands, Relation::Se};
	set_condition(cond, other);
	other->add_parent(this);

	other->set_translated();
	set_translated();
}

void Block::generate_sm(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto src = m_conditional_block->first.operands;
	if (is_num(src[0])) {
		set_reg(reg_map, src[0]);
		auto num = generate_num(std::stoull(src[0].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[0] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::COPY, src[0], {"A"}}));
	}
	else if (!reg_map[src[0]]) {
		set_reg(reg_map, src[0]);
		auto index = generate_index(src[0], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[0] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else
		src[0] = {std::string() + reg_map[src[0]]};

	if (is_num(src[1])) {
		auto num = generate_num(std::stoull(src[1].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[1] = {"A"};
	}
	else if (!reg_map[src[1]]) {
		auto index = generate_index(src[1], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[1] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else {
		src[1] = {std::string() + reg_map[src[1]]};
		m_commands.push_back(Command({}, Expression{Operator::COPY, Reference{"A"}, src[1]}));
		src[1] = {"A"};
	}

	m_commands.push_back(Command({}, Expression{Operator::SUB, src[1], src[0]}));
	std::swap(m_next_block, m_conditional_block->second);
	m_conditional_block->first.rel = Relation::Ge;
}

void Block::generate_gt(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto src = m_conditional_block->first.operands;
	if (is_num(src[1])) {
		set_reg(reg_map, src[1]);
		auto num = generate_num(std::stoull(src[1].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[1] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::COPY, src[1], {"A"}}));
	}
	else if (!reg_map[src[1]]) {
		set_reg(reg_map, src[1]);
		auto index = generate_index(src[1], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[1] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else
		src[1] = {std::string() + reg_map[src[1]]};

	if (is_num(src[0])) {
		auto num = generate_num(std::stoull(src[0].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[0] = {"A"};
	}
	else if (!reg_map[src[0]]) {
		auto index = generate_index(src[0], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[0] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else {
		src[0] = {std::string() + reg_map[src[0]]};
		m_commands.push_back(Command({}, Expression{Operator::COPY, Reference{"A"}, src[0]}));
		src[0] = {"A"};
	}

	m_commands.push_back(Command({}, Expression{Operator::SUB, src[0], src[1]}));

	std::swap(m_next_block, m_conditional_block->second);
	m_conditional_block->first.rel = Relation::Ge;
}

void Block::generate_se(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto src = m_conditional_block->first.operands;
	if (is_num(src[1])) {
		set_reg(reg_map, src[1]);
		auto num = generate_num(std::stoull(src[1].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[1] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::COPY, src[1], {"A"}}));
	}
	else if (!reg_map[src[1]]) {
		set_reg(reg_map, src[1]);
		auto index = generate_index(src[1], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[1] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else
		src[1] = {std::string() + reg_map[src[1]]};

	if (is_num(src[0])) {
		auto num = generate_num(std::stoull(src[0].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[0] = {"A"};
	}
	else if (!reg_map[src[0]]) {
		auto index = generate_index(src[0], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[0] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else {
		src[0] = {std::string() + reg_map[src[0]]};
		m_commands.push_back(Command({}, Expression{Operator::COPY, Reference{"A"}, src[0]}));
		src[0] = {"A"};
	}

	m_commands.push_back(Command({}, Expression{Operator::SUB, src[0], src[1]}));
}

void Block::generate_ge(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto src = m_conditional_block->first.operands;
	if (is_num(src[0])) {
		set_reg(reg_map, src[0]);
		auto num = generate_num(std::stoull(src[0].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[0] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::COPY, src[0], {"A"}}));
	}
	else if (!reg_map[src[0]]) {
		set_reg(reg_map, src[0]);
		auto index = generate_index(src[0], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[0] = {"B"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else
		src[0] = {std::string() + reg_map[src[0]]};

	if (is_num(src[1])) {
		auto num = generate_num(std::stoull(src[1].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[1] = {"A"};
	}
	else if (!reg_map[src[1]]) {
		auto index = generate_index(src[1], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[1] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[1]}));
	}
	else {
		src[1] = {std::string() + reg_map[src[1]]};
		m_commands.push_back(Command({}, Expression{Operator::COPY, Reference{"A"}, src[1]}));
		src[1] = {"A"};
	}

	m_commands.push_back(Command({}, Expression{Operator::SUB, src[1], src[0]}));
}

void Block::generate_odd(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program)
{
	auto& src = m_conditional_block->first.operands;
	if (is_num(src[0])) {
		auto num = generate_num(std::stoull(src[0].uuid));
		m_commands.insert(m_commands.end(), num.begin(), num.end());
		src[0] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::COPY, src[0], {"A"}}));
	}
	else if (!reg_map[src[0]]) {
		auto index = generate_index(src[0], reg_map, mem_map);
		m_commands.insert(m_commands.end(), index.begin(), index.end());
		src[0] = {"A"};
		m_commands.push_back(Command({}, Expression{Operator::LOAD, src[0]}));
	}
	else
		src[0] = {std::string() + reg_map[src[0]]};
}
