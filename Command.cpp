#include "Command.h"

bool operator<(const Reference lhs, const Reference rhs)
{
	return lhs.uuid < rhs.uuid;
}

bool operator==(const Reference& lhs, const Reference& rhs)
{
	if (lhs.uuid != rhs.uuid)
		return false;

	return true;
}

bool operator==(const StrongReference& lhs, const StrongReference& rhs)
{
	if (lhs.uuid != rhs.uuid)
		return false;

	if (lhs.version != rhs.version)
		return false;

	return true;
}

bool is_same(const Reference& lhs, const Reference& rhs)
{
	if (lhs.uuid != rhs.uuid)
		return false;

	if (lhs.offset != rhs.offset)
		return false;

	if (lhs.version != rhs.version)
		return false;

	return true;
}

bool is_binary(Operator op)
{
	if (op == Operator::Red)
		return false;

	if (op == Operator::Wrt)
		return false;

	if (op == Operator::Nul)
		return false;

	if (op == Operator::Def)
		return false;

	if (op == Operator::Und)
		return false;

	if (op == Operator::Half)
		return false;

	if (op == Operator::INC)
		return false;

	if (op == Operator::DEC)
		return false;

	if (op == Operator::HALF)
		return false;

	return true;
}

bool is_num(const Reference& ref)
{
	return std::isdigit(ref.uuid.front());
}

StrongReference::StrongReference(const Reference& ref) :
	Reference(ref)
{}

bool has_operands(Operator op)
{
	if (op == Operator::Red)
		return false;

	if (op == Operator::Def)
		return false;

	if (op == Operator::Und)
		return false;

	return true;
}

namespace std
{
string to_string(const Reference& ref)
{
	string result;
	result += ref.uuid;
	if (ref.offset)
		result += '[' + to_string(*ref.offset) + ']';
	if (ref.version != -1)
		result += '_' + std::to_string(ref.version);

	return result;
}

string to_string(const StrongReference& ref)
{
	string result;
	result += ref.uuid;
	if (ref.offset)
		result += '[' + to_string(*ref.offset) + ']';
	if (ref.version != -1)
		result += '_' + std::to_string(ref.version);

	return result;
}

string to_string(const Expression& expr)
{
	switch(expr.op) {
		case Operator::Nul:
			return to_string(expr.operands[0]);
		case Operator::Add:
			return to_string(expr.operands[0]) + " + " + to_string(expr.operands[1]);
		case Operator::Sub:
			return to_string(expr.operands[0]) + " - " + to_string(expr.operands[1]);
		case Operator::Mul:
			return to_string(expr.operands[0]) + " * " + to_string(expr.operands[1]);
		case Operator::Div:
			return to_string(expr.operands[0]) + " / " + to_string(expr.operands[1]);
		case Operator::Mod:
			return to_string(expr.operands[0]) + " % " + to_string(expr.operands[1]);
		case Operator::Phi:
			return "phi( " + to_string(expr.operands[0]) + ", " + to_string(expr.operands[1]) + " )";
		case Operator::Wrt:
			return "put " + to_string(expr.operands[0]);
		case Operator::Half:
			return "half( " + to_string(expr.operands[0]) + " )";
		case Operator::GET:
			return "GET " + expr.operands[0].uuid;
		case Operator::PUT:
			return "PUT " + expr.operands[0].uuid;
		case Operator::LOAD:
			return "LOAD " + expr.operands[0].uuid;
		case Operator::STORE:
			return "STORE " + expr.operands[0].uuid;
		case Operator::COPY:
			return "COPY " + expr.operands[0].uuid + " " + expr.operands[1].uuid;
		case Operator::ADD:
			return "ADD " + expr.operands[0].uuid + " " + expr.operands[1].uuid;
		case Operator::SUB:
			return "SUB " + expr.operands[0].uuid + " " + expr.operands[1].uuid;
		case Operator::HALF:
			return "HALF " + expr.operands[0].uuid;
		case Operator::INC:
			return "INC " + expr.operands[0].uuid;
		case Operator::DEC:
			return "DEC " + expr.operands[0].uuid;
		case Operator::JUMP:
			return "JUMP " + expr.operands[0].uuid;
		case Operator::JZERO:
			return "JZERO " + expr.operands[0].uuid + " " + expr.operands[1].uuid;
		case Operator::JODD:
			return "JODD " + expr.operands[0].uuid + " " + expr.operands[1].uuid;
		default:
			return "";
	}
}

string to_string(const Condition& cond)
{
	switch(cond.rel) {
		case Relation::Eq:
			return to_string(cond.operands[0]) + " == " + to_string(cond.operands[1]) + to_string(cond.propagated[0]) + " == " + to_string(cond.propagated[1]);
		case Relation::Ne:
			return to_string(cond.operands[0]) + " != " + to_string(cond.operands[1]) + to_string(cond.propagated[0]) + " != " + to_string(cond.propagated[1]);
		case Relation::Sm:
			return to_string(cond.operands[0]) + " < " + to_string(cond.operands[1]) + to_string(cond.propagated[0]) + " < " + to_string(cond.propagated[1]);
		case Relation::Gt:
			return to_string(cond.operands[0]) + " > " + to_string(cond.operands[1]) + to_string(cond.propagated[0]) + " > " + to_string(cond.propagated[1]);
		case Relation::Se:
			return to_string(cond.operands[0]) + " <= " + to_string(cond.operands[1]) + to_string(cond.propagated[0]) + " <= " + to_string(cond.propagated[1]);
		case Relation::Ge:
			return to_string(cond.operands[0]) + " >= " + to_string(cond.operands[1]) + to_string(cond.propagated[0]) + " >= " + to_string(cond.propagated[1]);
		case Relation::Odd:
			return "odd(" + to_string(cond.operands[0]) + ")" + to_string(cond.propagated[0]);
		default:
			return "";
	}
}
}

std::unordered_set<Reference> Command::get_inputs() const
{
	std::unordered_set<Reference> result;
	if (m_dest.offset)
		result.insert(*m_dest.offset);

	if (!has_operands(m_src.op))
			return result;

	result.insert(m_src.operands[0]);
	if (m_src.operands[0].offset)
		result.insert(*m_src.operands[0].offset);

	if (is_binary(m_src.op)) {
			result.insert(m_src.operands[1]);
			if (m_src.operands[1].offset)
				result.insert(*m_src.operands[1].offset);
	}

	return result;
}

std::unordered_set<StrongReference> Command::get_strong_inputs() const
{
	std::unordered_set<StrongReference> result;
	if (m_dest.offset)
		result.insert(*m_dest.offset);

	if (!has_operands(m_src.op))
			return result;

	result.insert(m_src.operands[0]);
	if (m_src.operands[0].offset)
		result.insert(*m_src.operands[0].offset);

	if (is_binary(m_src.op)) {
			result.insert(m_src.operands[1]);
			if (m_src.operands[1].offset)
				result.insert(*m_src.operands[1].offset);
	}

	return result;
}

std::unordered_set<Reference> Command::get_outputs() const
{
	std::unordered_set<Reference> result;
	if (m_src.op == Operator::Wrt)
		return result;

	result.insert(m_dest);

	return result;
}

std::unordered_set<StrongReference> Command::get_strong_outputs() const
{
	std::unordered_set<StrongReference> result;
	if (m_src.op == Operator::Wrt)
		return result;
	if (m_src.op == Operator::Und)
		return result;

	result.insert(m_dest);

	return result;
}

std::unordered_set<Reference> Command::get_undefs() const
{
	std::unordered_set<Reference> result;
	if (m_src.op != Operator::Und)
		return result;

	result.insert(m_dest);

	return result;
}

std::unordered_set<StrongReference> Command::get_connections() const
{
	std::unordered_set<StrongReference> result;
	if (m_src.op != Operator::Phi)
		return result;

	result.insert(m_src.operands[0]);
	result.insert(m_src.operands[1]);

	return result;
}

std::string Command::to_string() const
{
	std::string result;
	switch (m_src.op) {
		case Operator::Def:
			result = "define " + std::to_string(m_dest);
			break;
		case Operator::Und:
			result = "undefine " + std::to_string(m_dest);
			break;
		default:
			result = std::to_string(m_dest);
			result += " <- ";
			result += std::to_string(m_src);
			break;
	}

	result += "(" + std::to_string(m_propagated[0]) + ", " + std::to_string(m_propagated[1]) + ")";

	return result;
}

std::string Command::compile() const
{
	return std::to_string(m_src);
}

void Command::update_dest(std::unordered_set<Reference>& defs, std::unordered_set<Reference>& outs, const std::unordered_set<Reference>& consts)
{
	switch (m_src.op) {
		case Operator::Wrt:
			return;
		case Operator::Def:
		{
			auto inserted = outs.insert(m_dest);
			if (!inserted.second)
				throw std::runtime_error("\"" + m_dest.uuid + "\" already declared: on line " + std::to_string(m_line));

			defs.insert(m_dest);

			return;
		}
		case Operator::Und:
			outs.erase(m_dest);
			return;
		default:
			if (m_line != -1) {
				auto found = consts.find(m_dest);
				if (found != consts.end())
					throw std::runtime_error(m_dest.uuid + " is not modifiable: on line " + std::to_string(m_line));
			}
			
			auto found = defs.find(m_dest);
			if (found == defs.end())
				throw std::runtime_error("\"" + m_dest.uuid + "\" not declared: on line " + std::to_string(m_line));
			if (m_dest.offset)
				return;

			auto node = defs.extract(*found);
			++node.value().version;
			m_dest = node.value();
			defs.insert(std::move(node));

			found = outs.find(m_dest);
			node = outs.extract(*found);
			node.value().version = m_dest.version;
			outs.insert(std::move(node));

			return;
	}
}

void Command::update_src(std::unordered_set<Reference>& defs)
{
	switch (m_src.op) {
		case Operator::Def:
		{
			auto inserted = defs.insert(m_dest);
			return;
		}
		case Operator::Und:
			defs.erase(m_dest);
			return;
		default:
			break;
	}

	if (has_operands(m_src.op)) {
		if (is_binary(m_src.op) && !is_num(m_src.operands[1])) {
			auto offset = m_src.operands[1].offset;
			auto found = defs.find(m_src.operands[1]);
			if (found == defs.end())
				throw std::runtime_error("\"" + m_src.operands[1].uuid + "\" not declared: on line " + std::to_string(m_line));
			m_src.operands[1] = *found;
			if (offset && !is_num(*offset)) {
				auto found = defs.find(*offset);
				if (found == defs.end())
				throw std::runtime_error("\"" + offset->uuid + "\" not declared: on line " + std::to_string(m_line));
				offset = std::make_shared<Reference>(*found);
			}
			m_src.operands[1].offset = offset;
		}

		if (!is_num(m_src.operands[0])) {
			auto offset = m_src.operands[0].offset;
			auto found = defs.find(m_src.operands[0]);
			if (found == defs.end())
					throw std::runtime_error("\"" + m_src.operands[0].uuid + "\" not declared: on line " + std::to_string(m_line));
			m_src.operands[0] = *found;
			if (offset && !is_num(*offset)) {
				auto found = defs.find(*offset);
				if (found == defs.end())
					throw std::runtime_error("\"" + offset->uuid + "\" not declared: on line " + std::to_string(m_line));
				offset = std::make_shared<Reference>(*found);
			}
			m_src.operands[0].offset = offset;
		}
	}

	m_propagated = m_src.operands;

	if (m_src.op == Operator::Wrt)
		return;

	if ((m_dest.offset) && !is_num(*m_dest.offset)) {
		auto found = defs.find(*m_dest.offset);
		if (found == defs.end())
			throw std::runtime_error("\"" + m_dest.offset->uuid + "\" not declared: on line " + std::to_string(m_line));
		m_dest.offset = std::make_shared<Reference>(*found);
	}

	auto found = defs.find(m_dest);
	if (found == defs.end())
		throw std::runtime_error("\"" + m_dest.uuid + "\" not declared: on line " + std::to_string(m_line));
	auto node = defs.extract(*found);
	node.value().version = m_dest.version;
	defs.insert(std::move(node));
}

bool Command::update_phi(std::vector<std::unordered_set<Reference>> defs)
{
	if (m_src.op != Operator::Phi)
		return false;

	for (auto i = 0; i < 2; ++i) {
		auto inserted = defs[i].insert(m_src.operands[i]);
		m_src.operands[i] = *inserted.first;
	}

	m_propagated = m_src.operands;

	return true;
}

Operator Command::get_operator() const
{
	return m_src.op;
}

bool Command::is_complementary(const Command& other) const
{
	if (!(m_src.operands[0] == m_src.operands[0]))
		return false;

	if (!(m_src.operands[1] == m_src.operands[1]))
		return false;

	return true;
}

bool Command::is_a_reset() const
{
	if (m_src.operands[0].uuid != "A")
		return false;

	if (is_binary(m_src.op) && m_src.operands[1].uuid != "A")
		return false;

	return true;
}

std::unordered_set<StrongReference> Command::get_requrements()
{
	std::unordered_set<StrongReference> requirements;
	if (m_used) {
		if (has_operands(m_src.op)) {
			requirements.insert(m_src.operands[0]);
			if (m_src.operands[0].offset)
				requirements.insert(*m_src.operands[0].offset);
			if (is_binary(m_src.op)) {
				requirements.insert(m_src.operands[1]);
				if (m_src.operands[1].offset)
					requirements.insert(*m_src.operands[1].offset);
			}
		}
	}
	if (m_dest.offset)
		requirements.insert(*m_dest.offset);

	return requirements;
}

bool Command::check_requirement(StrongReference ref)
{
	if (static_cast<StrongReference>(m_dest) == ref && !m_used) {
		m_used = true;
		return true;
	}

	return false;
}

bool Command::is_used() const
{
	return m_used;
}
