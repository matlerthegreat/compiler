#pragma once

#include <string>
#include <unordered_set>
#include <unordered_map>

#include "Block.h"

class Program
{
public:
	Program();

	std::string to_string() const;
	void Convert_to_ssa();

	size_t register_block();
	void register_var(std::string pid, std::string start = "0", std::string end = "0");
	void register_const(std::string pid);

	Reference get_temporary();

	Flow_graph& get_graph();

	void propagate();
	void reduce();
	std::string translate();
	void un_dead();
	void optimize();
	std::vector<std::string> compile() const;

	std::unordered_set<Reference>& get_defs();
	std::unordered_set<Reference>& get_consts();

private:
	size_t m_free_block = 1;
	size_t m_free_mem = 1;
	std::unordered_map<std::string, std::array<size_t, 2>> m_var_table;
	std::unordered_set<std::string> m_array_table;
	std::unordered_set<Reference> m_const_table;
	std::unordered_set<Reference> m_defs;
	size_t m_free_tmp = 0;
	Flow_graph m_graph;
	RIG m_rig;
};

