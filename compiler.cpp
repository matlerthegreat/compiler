#include <iostream>
#include <fstream>

#include "parser/Parser.h"

int main(int argc, char* argv[])
{
	if (argc != 3) {
		std::cerr << "Usage: compiler input output" << std::endl;
		return 1;
	}

	std::ifstream f_in(argv[1]);

	Parser parser(f_in);
	if(parser.parse())
		return 1;

	auto program = parser.get_program();

	std::cerr << "### Three-addres code:" << std::endl;
	std::cerr << program.to_string() << std::endl;

	program.Convert_to_ssa();
	std::cerr << "### SSA code:" << std::endl;
	std::cerr << program.to_string() << std::endl;

	program.propagate();
	std::cerr << "### SSA code after propagation:" << std::endl;
	std::cerr << program.to_string() << std::endl;

	program.un_dead();
	std::cerr << "### SSA code after un_deading:" << std::endl;
	std::cerr << program.to_string() << std::endl;


	std::cerr << "### Code after reduction:" << std::endl;
	program.reduce();

	std::cerr << "### SSA code after reduction:" << std::endl;
	std::cerr << program.to_string() << std::endl;

	auto test = program.translate();
	std::cerr << "### Variable intereference:" << std::endl;
	std::cerr << test << std::endl;
	//program.optimize();

	auto code = program.compile();
	std::ofstream f_out(argv[2]);
	for (const auto& line: code) {
		f_out << line << std::endl;
	}

	return 0;
}
