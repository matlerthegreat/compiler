%baseclass-preinclude ../Program.h
%scanner ../scanner/Scanner.h
%polymorphic TEXT: std::string; REFERENCE: Reference; EXPRESSION: Expression; CONDITION: Condition; GRAPH: Flow_graph

%token DECLARE IN END
%token ASSIGN
%token NE SE GE
%token IF THEN ELSE ENDIF
%token WHILE DO ENDWHILE
%token ENDDO
%token FOR FROM TO DOWNTO ENDFOR
%token READ WRITE
%token PIDENTIFIER NUM
%nonassoc ';'
%left '+' '-'
%left '*' '/' '%'

%type <GRAPH> commands command
%type <CONDITION> condition
%type <REFERENCE> value identifier
%type <EXPRESSION> expression
%type <TEXT> pidentifier num

%%

program:
	DECLARE declarations IN commands END
	{
		auto& graph = m_program.get_graph();
		graph.leaf->merge($4.root);
		if ($4.leaf != $4.root)
			graph.leaf = $4.leaf;
	}
;

declarations:
	declarations pidentifier ';'
	{
		auto line = d_scanner.lineNr();
		std::vector<Command> commands;
		auto ref = Reference{$2};
		auto expr = Expression{Operator::Def};
		commands.push_back(Command(ref, expr, line));
		auto root = std::make_shared<Block>(commands);
		m_program.get_graph().leaf->merge(root);
		m_program.register_var($2);
	}
|
	declarations pidentifier '(' num ':' num ')' ';'
	{
		auto line = d_scanner.lineNr();
		std::vector<Command> commands;
		auto ref = Reference{$2};
		auto expr = Expression{Operator::Def};
		commands.push_back(Command(ref, expr, line));
		auto root = std::make_shared<Block>(commands);
		m_program.get_graph().leaf->merge(root);
		m_program.register_var($2, $4, $6);
	}
|
	//empty
;

commands:
	commands command
	{
		$$ = $1;
		$$.leaf->merge($2.root);
		if ($2.leaf != $2.root)
			$$.leaf = $2.leaf;
	}
|
	command
	{
		$$ = $1;
	}
;

command:
	identifier ASSIGN expression ';'
	{
		auto line = d_scanner.lineNr();
		std::vector<Command> commands = {Command($1, $3, line)};
		auto root = std::make_shared<Block>(commands);
		$$ = Flow_graph{root, root};
	}
|
	IF condition THEN commands ELSE commands ENDIF
	{
		std::vector<Command> phi_commands;
		std::unordered_set<Reference> inputs;
		auto outputs = $4.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		outputs = $6.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		for (auto input: inputs) {
			Expression expr = {Operator::Phi, input, input};
			phi_commands.push_back(Command(input, expr));
		}

		auto root = std::make_shared<Block>();
		auto if_block = std::make_shared<Block>(m_program.register_block());
		auto else_block = std::make_shared<Block>(m_program.register_block());
		auto leaf = std::make_shared<Block>(phi_commands, m_program.register_block());

		root->set_condition($2, if_block);
		root->set_next_block(else_block);

		$4.leaf->set_next_block(leaf);
		if_block->merge($4.root);
		if_block->add_parent(root.get());

		$6.leaf->set_next_block(leaf);
		else_block->merge($6.root);
		else_block->add_parent(root.get());

		if ($6.root != $6.leaf)
			leaf->add_parent($6.leaf.get());
		else
			leaf->add_parent(else_block.get());
		if ($4.root != $4.leaf)
			leaf->add_parent($4.leaf.get());
		else
			leaf->add_parent(if_block.get());

		$$ = Flow_graph{root, leaf};
	}
|
	IF condition THEN commands ENDIF
	{
		std::vector<Command> phi_commands;
		std::unordered_set<Reference> inputs;
		auto outputs = $4.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		for (auto input: inputs) {
			Expression expr = {Operator::Phi, input, input};
			phi_commands.push_back(Command(input, expr));
		}

		auto root = std::make_shared<Block>();
		auto if_block = std::make_shared<Block>(m_program.register_block());
		auto leaf = std::make_shared<Block>(phi_commands, m_program.register_block());

		root->set_condition($2, if_block);
		root->set_next_block(leaf);

		$4.leaf->set_next_block(leaf);
		if_block->merge($4.root);
		if_block->add_parent(root.get());

		if ($4.root != $4.leaf)
			leaf->add_parent($4.leaf.get());
		else
			leaf->add_parent(if_block.get());
		leaf->add_parent(root.get());

		$$ = Flow_graph{root, leaf};
	}
|
	WHILE condition DO commands ENDWHILE
	{
		std::vector<Command> phi_commands;
		std::unordered_set<Reference> inputs;
		auto outputs = $4.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		for (auto input: inputs) {
			Expression expr = {Operator::Phi, input, input};
			phi_commands.push_back(Command(input, expr));
		}

		auto root = std::make_shared<Block>();
		auto loop_header = std::make_shared<Block>(phi_commands, m_program.register_block());
		auto loop_body = std::make_shared<Block>(m_program.register_block());
		auto leaf = std::make_shared<Block>(m_program.register_block());

		root->set_next_block(loop_header);

		loop_header->set_condition($2, loop_body);
		loop_header->set_next_block(leaf);
		loop_header->add_parent(root.get());
		if ($4.root != $4.leaf)
			loop_header->add_parent($4.leaf.get());
		else
			loop_header->add_parent(loop_body.get());

		$4.leaf->set_next_block(loop_header);
		loop_body->merge($4.root);
		loop_body->add_parent(loop_header.get());

		leaf->add_parent(loop_header.get());

		$$ = Flow_graph{root, leaf};
	}

|
	DO commands WHILE condition ENDDO
	{
		std::vector<Command> phi_commands;
		std::unordered_set<Reference> inputs;
		auto outputs = $2.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		for (auto input: inputs) {
			Expression expr = {Operator::Phi, input, input};
			phi_commands.push_back(Command(input, expr));
		}

		auto root = std::make_shared<Block>();
		auto loop_body = std::make_shared<Block>(phi_commands, m_program.register_block());
		auto loop_footer = std::make_shared<Block>(m_program.register_block());
		auto leaf = std::make_shared<Block>(m_program.register_block());

		root->set_next_block(loop_body);

		$2.leaf->set_next_block(loop_footer);
		loop_body->merge($2.root);
		loop_body->add_parent(root.get());
		loop_body->add_parent(loop_footer.get());

		loop_footer->set_condition($4, loop_body);
		loop_footer->set_next_block(leaf);
		if ($2.root != $2.leaf)
			loop_footer->add_parent($2.leaf.get());
		else
			loop_footer->add_parent(loop_body.get());

		leaf->add_parent(loop_footer.get());

		$$ = Flow_graph{root, leaf};
	}
|
	FOR pidentifier FROM value TO value DO commands ENDFOR
	{
		auto tmp = m_program.get_temporary();
		m_program.register_var(tmp.uuid);
		auto iterator = Reference{$2};
		m_program.register_const($2);
		auto one = Reference{"1"};

		std::vector<Command> root_commands;
		Expression expr = {Operator::Def};
		root_commands.push_back(Command(iterator, expr)); // def iterator
		root_commands.push_back(Command(tmp, expr)); // def temp
		expr = {Operator::Nul, $6};
		root_commands.push_back(Command(tmp, expr)); // tmp <- end
		expr = {Operator::Nul, $4};
		root_commands.push_back(Command(iterator, expr)); // iterator <- begin

		std::vector<Command> leaf_commands;
		expr = {Operator::Und};
		leaf_commands.push_back(Command(tmp, expr)); // undef temp
		leaf_commands.push_back(Command(iterator, expr)); // undef iterator

		std::vector<Command> loop_footer_commands;
		expr = {Operator::Add, iterator, one};
		loop_footer_commands.push_back(Command(iterator, expr)); // iterator <- iterator + 1

		std::vector<Command> phi_commands;
		std::unordered_set<Reference> inputs;
		auto outputs = $8.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		expr = {Operator::Phi, iterator, iterator};
		phi_commands.push_back(Command(iterator, expr));
		for (auto input: inputs) {
			expr = {Operator::Phi, input, input};
			phi_commands.push_back(Command(input, expr));
		}

		auto root = std::make_shared<Block>(root_commands);
		auto loop_header = std::make_shared<Block>(phi_commands, m_program.register_block());
		auto loop_body = std::make_shared<Block>(m_program.register_block());
		auto loop_footer = std::make_shared<Block>(loop_footer_commands);
		auto leaf = std::make_shared<Block>(leaf_commands, m_program.register_block());

		root->set_next_block(loop_header);

		auto cond = Condition{{iterator, tmp}, Relation::Se};
		loop_header->set_condition(cond, loop_body);
		loop_header->set_next_block(leaf);
		loop_header->add_parent(root.get());
		if ($8.leaf != $8.root)
			loop_header->add_parent($8.leaf.get());
		else
			loop_header->add_parent(loop_body.get());

		$8.leaf->merge(loop_footer);
		$8.leaf->set_next_block(loop_header);
		loop_body->merge($8.root);
		loop_body->add_parent(loop_header.get());

		leaf->add_parent(loop_header.get());

		$$ = Flow_graph{root, leaf};
	}
|
	FOR pidentifier FROM value DOWNTO value DO commands ENDFOR
	{
		auto tmp = m_program.get_temporary();
		m_program.register_var(tmp.uuid);
		auto iterator = Reference{$2};
		m_program.register_const($2);
		auto one = Reference{"1"};

		std::vector<Command> root_commands;
		Expression expr = {Operator::Def};
		root_commands.push_back(Command(tmp, expr)); // def temp
		root_commands.push_back(Command(iterator, expr)); // def iterator
		expr = {Operator::Nul, $6};
		root_commands.push_back(Command(tmp, expr)); // tmp <- end
		expr = {Operator::Nul, $4};
		root_commands.push_back(Command(iterator, expr)); // iterator <- begin

		std::vector<Command> leaf_commands;
		expr = {Operator::Und};
		leaf_commands.push_back(Command(tmp, expr)); // undef temp
		leaf_commands.push_back(Command(iterator, expr)); // undef iterator

		std::vector<Command> loop_footer_commands;
		expr = {Operator::Sub, iterator, one};
		loop_footer_commands.push_back(Command(iterator, expr)); // iterator <- iterator + 1

		std::vector<Command> phi_commands;
		std::unordered_set<Reference> inputs;
		auto outputs = $8.leaf->get_outputs();
		inputs.insert(outputs.begin(), outputs.end());
		expr = {Operator::Phi, iterator, iterator};
		phi_commands.push_back(Command(iterator, expr));
		for (auto input: inputs) {
			expr = {Operator::Phi, input, input};
			phi_commands.push_back(Command(input, expr));
		}


		auto root = std::make_shared<Block>(root_commands);
		auto loop_header = std::make_shared<Block>(phi_commands, m_program.register_block());
		auto loop_body = std::make_shared<Block>(m_program.register_block());
		auto loop_footer = std::make_shared<Block>(loop_footer_commands, m_program.register_block());
		auto leaf = std::make_shared<Block>(leaf_commands, m_program.register_block());

		root->set_next_block(loop_header);

		auto cond = Condition{{iterator, tmp}, Relation::Ge};
		loop_header->set_condition(cond, loop_body);
		loop_header->set_next_block(leaf);
		loop_header->add_parent(root.get());

		$8.leaf->set_next_block(loop_footer);
		if ($8.root != $8.leaf)
			loop_footer->add_parent($8.leaf.get());
		else
			loop_footer->add_parent(loop_body.get());
		cond = Condition{{iterator, Reference{"0"}}, Relation::Se};
		$8.leaf->set_condition(cond, leaf);
		if ($8.root != $8.leaf)
			leaf->add_parent($8.leaf.get());
		else
			leaf->add_parent(loop_body.get());
		loop_footer->set_next_block(loop_header);
		loop_header->add_parent(loop_footer.get());
		//$8.leaf->merge(loop_footer);
		//$8.leaf->set_next_block(loop_header);
		//$8.leaf->set_condition(cond, leaf);
		loop_body->merge($8.root);
		loop_body->add_parent(loop_header.get());

		leaf->add_parent(loop_header.get());

		$$ = Flow_graph{root, leaf};
	}
|
	READ identifier ';'
	{
		auto line = d_scanner.lineNr();
		auto expr = Expression{Operator::Red};
		std::vector<Command> commands = {Command($2, expr, line)};
		auto root = std::make_shared<Block>(commands);
		$$ = Flow_graph{root, root};
	}
|
	WRITE value ';'
	{
		auto line = d_scanner.lineNr();
		auto expr = Expression{Operator::Wrt, $2};
		std::vector<Command> commands = {Command(Reference{}, expr, line)};
		auto root = std::make_shared<Block>(commands);
		$$ = Flow_graph{root, root};
	}
;

expression:
	value
	{
		$$ = Expression{Operator::Nul, $1};
	}
|
	value '+' value
	{
		$$ = Expression{Operator::Add, $1, $3};
	}

|
	value '-' value
	{
		$$ = Expression{Operator::Sub, $1, $3};
	}
|
	value '*' value
	{
		$$ = Expression{Operator::Mul, $1, $3};
	}
|
	value '/' value
	{
		$$ = Expression{Operator::Div, $1, $3};
	}
|
	value '%' value
	{
		$$ = Expression{Operator::Mod, $1, $3};
	}
;

condition:
	value '=' value
	{
		$$ = Condition{{$1, $3}, Relation::Eq};
	}
|
	value NE value
	{
		$$ = Condition{{$1, $3}, Relation::Ne};
	}
|
	value '<' value
	{
		$$ = Condition{{$1, $3}, Relation::Sm};
	}
|
	value '>' value
	{
		$$ = Condition{{$1, $3}, Relation::Gt};
	}
|
	value SE value
	{
		$$ = Condition{{$1, $3}, Relation::Se};
	}
|
	value GE value
	{
		$$ = Condition{{$1, $3}, Relation::Ge};
	}
;

value:
	num
	{
		$$ = Reference{$1};
	}
|
	identifier
;

identifier:
	pidentifier
	{
		$$ = Reference{$1};
	}
|
	pidentifier '(' pidentifier ')'
	{
		$$ = Reference{$1, std::make_shared<Reference>(Reference{$3})};
	}
|
	pidentifier '(' num ')'
	{
		$$ = Reference{$1, std::make_shared<Reference>(Reference{$3})};
	}
;

num:
	NUM
	{
		$$ = d_scanner.matched();
	}
;

pidentifier:
	PIDENTIFIER
	{
		$$ = d_scanner.matched();
	}
;
