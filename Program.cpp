#include "Program.h"

#include <iostream>

Program::Program() :
	m_graph{std::make_shared<Block>(0)}
{
	m_graph.leaf = m_graph.root;
}

std::string Program::to_string() const
{
	return m_graph.root->to_string();
}

void Program::Convert_to_ssa()
{
	std::unordered_set<Reference> defs = {};
	m_graph.root->update_version(defs, m_const_table);
}

size_t Program::register_block()
{
	return m_free_block++;
}

void Program::register_var(std::string pid, std::string start, std::string end)
{
	size_t offset = std::stoll(start);

	m_var_table.insert({pid, {m_free_mem, offset}});

	size_t size = std::stoll(end) - offset + 1;
	m_free_mem += size;
}

void Program::register_const(std::string pid)
{
	m_const_table.insert(Reference{pid});
	m_var_table.insert({pid, {m_free_mem, 0}});

	m_free_mem += 1;
}

Reference Program::get_temporary()
{
	return Reference{"$temp" + std::to_string(m_free_tmp++)};
}

Flow_graph& Program::get_graph()
{
	return m_graph;
}

void Program::propagate()
{
	m_graph.root->propagate();
}

void Program::un_dead()
{
	m_graph.root->un_dead();

	std::unordered_set<Reference> defs = {};
	m_graph.root->update_version(defs, m_const_table);
}

void Program::reduce()
{
	std::unordered_set<const Block*> visited;
	m_graph.root->reduce(visited, *this);

	std::unordered_set<Reference> defs = {};
	m_graph.root->update_version(defs, m_const_table);
}

std::string Program::translate()
{
	auto rig = m_graph.root->generate_rig();
	auto intereference = rig.get_interference();
	rig.allocate_registers();

	std::cerr << "Register map: " << std::endl;
	auto reg_map = rig.get_reg_map();
	for( const auto& n : reg_map ) {
        std::cerr << "Key:[" << std::to_string(n.first) << "] Value:[" << n.second << "]\n";
    }

	std::unordered_set<const Block*> visited;
	m_graph.root->translate(visited, reg_map, m_var_table, *this);

	return intereference;
}

void Program::optimize()
{
	m_graph.root->optimize();
}

std::vector<std::string> Program::compile() const
{
	return m_graph.root->compile();
}
