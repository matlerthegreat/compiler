#pragma once
#include <optional>
#include <string>
#include <array>
#include <vector>
#include <memory>
#include <unordered_set>
#include <unordered_map>

struct Reference;
struct StrongReference;
struct Expression;
bool operator==(const StrongReference& lhs, const StrongReference& rhs);
bool operator==(const Reference& lhs, const Reference& rhs);
bool is_same(const Reference& lhs, const Reference& rhs);
bool is_num(const Reference& ref);
using ULLONG = unsigned long long;

struct Reference
{
	Reference() = default;
	Reference(const StrongReference&) = delete;
	Reference(StrongReference&) = delete;
	Reference(StrongReference) = delete;
	std::string uuid;
	std::shared_ptr<Reference> offset;
	size_t version = -1;
};

struct StrongReference: public Reference
{
	StrongReference(const Reference& ref);
};

enum class Operator
{
	Nul,
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	Red,
	Wrt,
	Def,
	Und,
	Phi,
	Half,
	GET,
	PUT,
	LOAD,
	STORE,
	COPY,
	ADD,
	SUB,
	HALF,
	INC,
	DEC,
	JUMP,
	JZERO,
	JODD
};

bool is_binary(Operator op);
bool has_operands(Operator op);
struct Expression
{
	Operator op;
	std::array<Reference, 2> operands;
};

enum class Relation
{
	Eq,
	Ne,
	Sm,
	Gt,
	Se,
	Ge,
	Odd
};

struct Condition
{
	std::array<Reference, 2> operands;
	Relation rel;
	std::array<Reference, 2> propagated;
};

namespace std
{
string to_string(const Reference& ref);
string to_string(const StrongReference& ref);
string to_string(const Expression& expr);
string to_string(const Condition& cond);
template<> struct hash<Reference>
	{
		typedef Reference argument_type;
		typedef std::size_t result_type;
		result_type operator()(argument_type const& s) const noexcept
		{
			return std::hash<std::string>{}(s.uuid);
		}
	};
template<> struct hash<StrongReference>
	{
		typedef StrongReference argument_type;
		typedef std::size_t result_type;
		result_type operator()(argument_type const& s) const noexcept
		{
			auto h1 = std::hash<std::string>{}(s.uuid);
			auto h2 = std::hash<size_t>{}(s.version);

			return h1 ^ (h2 << 1);
		}
	};
}

class Block;
struct Flow_graph
{
	std::shared_ptr<Block> root;
	std::shared_ptr<Block> leaf;
};

class Program;

class Command
{
public:
	Command(Reference dest, Expression src, size_t line = -1) :
		m_dest(dest),
		m_src(src),
		m_propagated(src.operands),
		m_line(line)
	{
		switch (m_src.op) {
			case Operator::Def:
			case Operator::Und:
			case Operator::Wrt:
			case Operator::Red:
			m_used = true;
		}

		if (m_dest.offset)
			m_used = true;
	}

	std::string to_string() const;
	std::string compile() const;

	std::unordered_set<Reference> get_inputs() const;
	std::unordered_set<StrongReference> get_strong_inputs() const;
	std::unordered_set<Reference> get_outputs() const;
	std::unordered_set<StrongReference> get_strong_outputs() const;
	std::unordered_set<Reference> get_undefs() const;
	std::unordered_set<StrongReference> get_connections() const;
	std::optional<std::array<StrongReference, 2>> get_replacement();
	void replace(std::array<StrongReference, 2> replacement);

	void translate();
	Flow_graph generate(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program) const;
	Flow_graph reduce(Program& program) const;

	void update_dest(std::unordered_set<Reference>& defs, std::unordered_set<Reference>& outs, const std::unordered_set<Reference>& consts);
	void update_src(std::unordered_set<Reference>& defs);

	bool update_phi(std::vector<std::unordered_set<Reference>> defs);
	Operator get_operator() const;
	bool is_complementary(const Command& other) const;
	bool is_a_reset() const;
	bool is_used() const;
	std::unordered_set<StrongReference> get_requrements();
	bool check_requirement(StrongReference ref);

private:
	Flow_graph generate_nul(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const;
	Flow_graph generate_add(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const;
	Flow_graph generate_sub(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const;
	Flow_graph generate_mul(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map, Program& program) const;
	Flow_graph generate_red(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const;
	Flow_graph generate_wrt(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const;
	Flow_graph generate_half(std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map) const;

	Flow_graph reduce_mul(Program& program) const;
	Flow_graph reduce_div(Program& program) const;
	Flow_graph reduce_mod(Program& program) const;

	std::optional<std::array<StrongReference, 2>> calc_nul();
	std::optional<std::array<StrongReference, 2>> calc_add();
	std::optional<std::array<StrongReference, 2>> calc_sub();
	std::optional<std::array<StrongReference, 2>> calc_mul();
	std::optional<std::array<StrongReference, 2>> calc_div();
	std::optional<std::array<StrongReference, 2>> calc_mod();


	Reference m_dest;
	Expression m_src;
	std::array<Reference, 2> m_propagated;
	size_t m_line;
	bool m_used = false;
};

std::vector<Command> generate_num(ULLONG num, Reference reg = {"A"});
std::vector<Command> generate_index(Reference ref, std::unordered_map<StrongReference, char>& reg_map, std::unordered_map<std::string, std::array<size_t, 2>> mem_map);
void set_reg(std::unordered_map<StrongReference, char>& reg_map, Reference ref = {""}, char reg = 'B');
