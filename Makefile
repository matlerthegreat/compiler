kompilator: compiler.cpp Command.cpp Block.cpp Program.cpp RIG.cpp Translate.cpp Reduce.cpp Propagate.cpp scanner/lex.cc parser/parse.cc
	g++ -std=gnu++2a -o kompilator compiler.cpp Command.cpp Block.cpp Program.cpp RIG.cpp Translate.cpp Reduce.cpp Propagate.cpp scanner/lex.cc parser/parse.cc

parser/parse.cc:
	$(MAKE) -C parser

scanner/lex.cc:
	$(MAKE) -C scanner
