#pragma once

#include <unordered_set>
#include <unordered_map>
#include <string>

#include "Command.h"

struct RIG_node
{
	std::unordered_set<StrongReference> connects;
	std::unordered_set<StrongReference> inters;
	char reg = 0;
};

class RIG
{
public:
	RIG();

	void add_ref(StrongReference ref);
	void add_inter(StrongReference lhs, StrongReference rhs);
	void add_conect(StrongReference lhs, StrongReference rhs);

	void allocate_registers();
	char get_register(Reference ref);
	std::unordered_map<StrongReference, char> get_reg_map() const;

	std::string get_interference() const;

private:
	std::vector<RIG_node> m_graph;
	std::unordered_map<StrongReference, char> m_reg_map;
};
