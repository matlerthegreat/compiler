%x comment

%%

[ \t]+		// skip whitespaces

"["			begin(StartCondition_::comment);
<comment>"]"	begin(StartCondition_::INITIAL);
<comment>.		// ignored

"DECLARE"	return Parser::DECLARE;
"IN"		return Parser::IN;
"END"		return Parser::END;

"IF"		return Parser::IF;
"THEN"		return Parser::THEN;
"ELSE"		return Parser::ELSE;
"ENDIF"		return Parser::ENDIF;

"WHILE"		return Parser::WHILE;
"DO"		return Parser::DO;
"ENDWHILE"	return Parser::ENDWHILE;

"ENDDO"		return Parser::ENDDO;

"FOR"		return Parser::FOR;
"FROM"		return Parser::FROM;
"TO"		return Parser::TO;
"DOWNTO"	return Parser::DOWNTO;
"ENDFOR"	return Parser::ENDFOR;

"READ"		return Parser::READ;
"WRITE"		return Parser::WRITE;

":="		return Parser::ASSIGN;

"!="		return Parser::NE;
"<="		return Parser::SE;
">="		return Parser::GE;

[_a-z]+		return Parser::PIDENTIFIER;
[0-9]+		return Parser::NUM;

.			return matched()[0];
