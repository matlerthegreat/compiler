#include "RIG.h"

#include <algorithm>
#include <set>

RIG::RIG()
{
	std::unordered_set<StrongReference> connects = {Reference{"$tab"}};
	m_graph.push_back(RIG_node{connects});
	m_graph.back().reg = 'H';
}

void RIG::add_ref(StrongReference ref)
{
	if (ref.version == -1)
		return;

	std::unordered_set<StrongReference> connects = {ref};
	m_graph.push_back(RIG_node{connects});
}

void RIG::add_inter(StrongReference lhs, StrongReference rhs)
{
	if (lhs.offset) {
		lhs.uuid = "$tab";
	}
	else if (lhs.version == -1)
		return;

	if (rhs.offset) {
		rhs.uuid = "$tab";
	}
	else if (rhs.version == -1)
		return;

	auto first = std::find_if(m_graph.begin(), m_graph.end(), [lhs](const RIG_node& node) {
				return node.connects.count(lhs) > 0; });
	if (first == m_graph.end())
		throw 3;


	auto second = std::find_if(m_graph.begin(), m_graph.end(), [rhs](const RIG_node& node) {
				return node.connects.count(rhs) > 0; });
	if (second == m_graph.end())
		throw 3;

	first->inters.insert(second->connects.begin(), second->connects.end());
	second->inters.insert(first->connects.begin(), first->connects.end());
}

void RIG::add_conect(StrongReference lhs, StrongReference rhs)
{
	if (lhs.offset) {
		lhs.uuid = "$tab";
	}
	else if (lhs.version == -1)
		return;

	if (rhs.offset) {
		rhs.uuid = "$tab";
	}
	else if (rhs.version == -1)
		return;

	auto first = std::find_if(m_graph.begin(), m_graph.end(), [lhs, rhs](const RIG_node& node) {
			return node.connects.count(lhs) > 0; });
	if (first == m_graph.end())
		throw 3;

	auto second = std::find_if(m_graph.begin(), m_graph.end(), [lhs, rhs](const RIG_node& node) {
			return node.connects.count(rhs) > 0; });
	if (second == m_graph.end())
		throw 2;

	if (first == second)
		return;

	for (auto& inter: first->inters) {
		auto third = std::find_if(m_graph.begin(), m_graph.end(), [lhs, rhs, inter](const RIG_node& node) {
			return node.connects.count(inter) > 0; });
	if (third == m_graph.end())
		throw 1;

		third->inters.insert(second->connects.begin(), second->connects.end());
	}

	for (auto& inter: second->inters) {
		auto third = std::find_if(m_graph.begin(), m_graph.end(), [lhs, rhs, inter](const RIG_node& node) {
			return node.connects.count(inter) > 0; });
	if (third == m_graph.end())
		throw 1;

		third->inters.insert(first->connects.begin(), first->connects.end());
	}

	first->connects.insert(second->connects.begin(), second->connects.end());
	first->inters.insert(second->inters.begin(), second->inters.end());

	for (auto& inter: second->inters) {
		auto third = std::find_if(m_graph.begin(), m_graph.end(), [lhs, rhs, inter](const RIG_node& node) {
			return node.connects.count(inter) > 0; });

		third->inters.insert(first->connects.begin(), first->connects.end());
	}

	m_graph.erase(second);
	/*std::cerr << "after: " << std::endl;
	for (auto& node: m_graph) {
		std::cerr<< "{";
		for (auto& conn: node.connects) {
			std::cerr  << std::to_string(conn) << ", ";
		}
		std::cerr<< "}" << std::endl;
	}
	std::cerr << std::endl << std::endl;*/
}

void RIG::allocate_registers()
{
	std::vector<RIG_node> stack;
	auto tab_var = m_graph.front();
	m_graph.erase(m_graph.begin());

	// transfer nodes to stack
	while (!m_graph.empty()) {
		// find node with least number of connections
		std::vector<RIG_node>::iterator found;
		auto max = 0;
		for (auto it = m_graph.begin(); it != m_graph.end(); ++it) {
			std::set<std::vector<RIG_node>::const_iterator> connections;
			for (auto& inter: it->inters) {
				auto second = std::find_if(m_graph.begin(), m_graph.end(), [inter](const RIG_node& node) {
					return node.connects.count(inter) > 0; });

				connections.insert(second);
			}
			if (connections.size() < 5) {
				found = it;
				break;
			}
			if (connections.size() > max) {
				found = it;
				max = connections.size();
			}
		}
		
		//move node to stack
		stack.push_back(*found);
		m_graph.erase(found);

		// remove connections from other nodes in graph
		for (auto& node: m_graph) {
			for (const auto& connect: stack.back().connects) {
				node.inters.erase(connect);
			}
		}
	}

	stack.push_back(tab_var);

	// add nodes back to graph
	while (!stack.empty()) {
		if (stack.back().reg) {
			m_graph.push_back(stack.back());
			stack.pop_back();
			continue;
		}
		std::unordered_set<char> registers = {'C', 'D', 'E', 'F', 'G', 'H'};
		//std::unordered_set<char> registers = {};

		for (auto& node: m_graph) {
			for (auto inter: stack.back().inters) {
				if (node.connects.count(inter) > 0)
					registers.erase(node.reg);
			}
		}

		auto reg = registers.begin();
		if (reg != registers.end())
			stack.back().reg = *reg;

		for (auto&ref: stack.back().connects) {
			m_reg_map[ref] = stack.back().reg;
		}

		m_graph.push_back(stack.back());
		stack.pop_back();
	}
}

std::unordered_map<StrongReference, char> RIG::get_reg_map() const
{
	return m_reg_map;
}

char RIG::get_register(Reference ref)
{
	auto found = std::find_if(m_graph.begin(), m_graph.end(), [ref](const RIG_node& node) {
				return node.connects.count(ref) > 0; });

	return found->reg;
}

std::string RIG::get_interference() const
{
	std::string result;
	for (const auto& node: m_graph) {
		result += '{';

		for (const auto& conn: node.connects)
			result += conn.uuid + "_" + std::to_string(conn.version) + ", ";

		result += "}: {";

		for (const auto& conn: node.inters)
			result += conn.uuid + "_" + std::to_string(conn.version) + ", ";

		result += "}\n";
	}

	return result;
}
